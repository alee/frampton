/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <gio/gio.h>
#include "frampton-agent-iface.h"
#include "frampton-agent.h"
#include "tinwell.h"
#include "libgrassmoor-tracker.h"

#define CLIENT_NAME FRAMPTON_BUNDLE_ID ".Test"

static FramptonAgent *proxyPlayer;
static void 
play_started_cb( FramptonAgent *object,
    const gchar *arg_app_name,
    const gchar *arg_track_name)
{
	g_print("%s %s\n",__FUNCTION__,arg_track_name);
}
static void 
play_completed_cb( FramptonAgent *object,
    const gchar *arg_app_name,
    const gchar *arg_track_name)
{
	g_print("%s %s\n",__FUNCTION__,arg_track_name);
}
static void 
play_paused_cb( FramptonAgent *object,
    const gchar *arg_app_name,
    const gchar *arg_track_name,
    gdouble arg_track_pos)
{
	g_print("%s %s %lf\n",__FUNCTION__,arg_track_name,arg_track_pos);
}
static void 
play_resumed_cb(
FramptonAgent *object,
    const gchar *arg_app_name,
    const gchar *arg_track_name,
    gdouble arg_track_pos)
{
	g_print("%s %s %lf\n",__FUNCTION__,arg_track_name,arg_track_pos);
}
static void 
play_position_cb(
FramptonAgent *object,
    const gchar *arg_app_name,
    const gchar *arg_track_name,
    gdouble arg_track_pos)
{
	g_print("%s %s %lf\n",__FUNCTION__,arg_track_name,arg_track_pos);
}
static void 
playback_error_cb( FramptonAgent *object,
    const gchar *arg_app_name,
    const gchar *arg_track_name,
    const gchar *arg_err_msg)
{
	g_print("%s\n",__FUNCTION__);
}
static void
repeat_message_info_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
	GError *error = NULL;
	g_print("%s\n",__FUNCTION__);
	frampton_agent_call_set_repeat_state_finish((FramptonAgent *)source_object,res,&error);
}

static void
paly_message_info_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
	GError *error = NULL;
	g_print("%s\n",__FUNCTION__);
	frampton_agent_call_start_playback_finish((FramptonAgent *)source_object,res,&error);
	//frampton_agent_call_track_seek(proxyPlayer,CLIENT_NAME,0.900000L,NULL,seek_info_cb,NULL);
}
static void
get_url_message_info_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
	gchar** urlList = NULL;
	gchar** modeList = NULL;
	
	GError *error = NULL;
	gint len = 0;
	gint modeUrlLen = 0;
	g_print("%s\n",__FUNCTION__);
	frampton_agent_call_get_track_list_finish(proxyPlayer,&urlList,&len, &modeList, &modeUrlLen, res,&error);
	if(error)
	{
		g_print("error %s \n",error->message);

	}
	g_print("%s\n",__FUNCTION__);
	if(urlList)
	{
		gint i = 0;

		g_print("%s\n",__FUNCTION__);
		while (urlList[i])
		{
			g_print("%s\n",__FUNCTION__);
			g_print("total length = %s\n",urlList[i]);
			i++;
		}
		frampton_agent_call_set_shuffle_state(proxyPlayer,CLIENT_NAME,TRUE,NULL,repeat_message_info_cb,NULL);
		frampton_agent_call_start_playback(proxyPlayer,CLIENT_NAME,urlList[0],NULL,paly_message_info_cb,NULL);
	}
}
static void
registration_info_message_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
	gboolean regState;

	frampton_agent_call_register_finish(proxyPlayer, &regState, res , user_data);
	if(regState)
	{
		g_print("registration for %s to Audio service is successfull \n",CLIENT_NAME);
		frampton_agent_call_get_track_list(proxyPlayer,CLIENT_NAME,"local",FRAMPTON_AGENT_SORT_DESCENDING, FRAMPTON_AGENT_MEDIA_INFO_ALBUM, NULL,get_url_message_info_cb,NULL);
	}
	else
		g_print("registration for %s to Audio service is failled! \n",CLIENT_NAME);
}
#if 0
static void unregistration_info_message_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
        gboolean regState;

    frampton_audio_agent_call_unregister_audio_agent_finish(proxyPlayer, &regState, res , user_data);

        if(TRUE == regState)
                exit(1);
}
static void
audio_client_unregister(void )
{
        frampton_audio_agent_call_unregister_audio_agent(proxyPlayer,CLIENT_NAME, NULL, unregistration_info_message_cb, NULL);

}
#endif
static void
termination_handler (int signum)
{
        exit(0);
}
static void register_exit_functionalities(void)
{
        struct sigaction action;
        action.sa_handler = termination_handler;
        action.sa_flags = 0;
        sigaction(SIGINT, &action, NULL);
        sigaction(SIGHUP, &action, NULL);
        sigaction(SIGTERM, &action, NULL);
        //atexit(audio_client_unregister);
}
static void audio_agent_service_proxy_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
        GError *error;

        /* finishes the proxy creation and gets the proxy ptr */
        proxyPlayer = frampton_agent_proxy_new_finish(res , &error);

        if(proxyPlayer == NULL)
        {
        }
        else
        {
                g_signal_connect(proxyPlayer, "play-started", G_CALLBACK(play_started_cb), NULL);
                g_signal_connect(proxyPlayer, "play-completed", G_CALLBACK(play_completed_cb), NULL);
                g_signal_connect(proxyPlayer, "play-paused", G_CALLBACK(play_paused_cb), NULL);
                g_signal_connect(proxyPlayer, "play-resumed", G_CALLBACK(play_resumed_cb), NULL);
                g_signal_connect(proxyPlayer, "play-position", G_CALLBACK(play_position_cb), NULL);
                g_signal_connect(proxyPlayer, "play-error", G_CALLBACK(playback_error_cb), NULL);
                //register for exit functionalities
                register_exit_functionalities();

                frampton_agent_call_register(proxyPlayer, CLIENT_NAME, "icon_path",NULL, registration_info_message_cb, NULL);
        }
}
static void name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
        g_print("\n Name Vanished");
        if(NULL != proxyPlayer)
    g_object_unref(proxyPlayer);
}

static void name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
  /* Asynchronously creates a proxy for the D-Bus interface */
  frampton_agent_proxy_new (connection,
                                         G_DBUS_PROXY_FLAGS_NONE,
                                         "Frampton.Agent",
                                         "/Frampton/Agent",
                                          NULL,
                                          audio_agent_service_proxy_cb,
                                          NULL);
}
gint main(gint argc, gchar *argv[])
{

        GMainLoop *mainloop = NULL;

        gint watch;

        /* Starts watching name on the bus specified by bus_type */
        /* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
        watch = g_bus_watch_name (G_BUS_TYPE_SESSION,
                        "Frampton.Agent",
                        G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                        name_appeared,
                        name_vanished,
                        NULL,
                        NULL);

        mainloop = g_main_loop_new (NULL, FALSE);
        g_main_loop_run (mainloop);

        exit (0);
}
