/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/***********************************************************************************
*       @Filename       :       frampton-agent-main.c
*       @Module         :      	Service
*       @Project        :      	Frampton
*----------------------------------------------------------------------------------
*       @Copyright      :       
*       @Authors        :
*       @Date           :
*----------------------------------------------------------------------------------
*       @Description    :       The basic idea of agent service is to provide the
*								backend for audio player UI.
*
************************************************************************************
Description of FIXES:
-----------------------------------------------------------------------------------

***********************************************************************************
        @System Includes
************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gio/gio.h>
#include <glib-unix.h>
#include <glib.h>
#include <glib/gstdio.h>

#include "prestwood.h"
#include "tinwell.h"
#include "mildenhall-statusbar.h"
#include "libgrassmoor-tracker.h"
#include "seaton-preference.h"

/***********************************************************************************
        @Project Includes
************************************************************************************/
#include "frampton-agent.h"
#include "frampton-service.h"

/***********************************************************************************
        @Prototypes of local functions
************************************************************************************/

/***********************************************************************************
        @Macro Definitions
************************************************************************************/
#define AGENT_WIDGET_NAME "_AW"

typedef struct _FramptonAgentInternalData FramptonAgentInternalData;
struct _FramptonAgentInternalData
{
        gdouble trackPos;
        gboolean bRepeat;
        gboolean bShuffle;
        gchar *appName;
        gchar *curPlayingTrack;
        gchar *pIconPath;
        gchar *volumeMountPoint;
        GPtrArray *songsList;
        gchar **pArrStrArtistList;
        gchar **pArrStrAlbumList;

        enPlayState playState;
        gchar **pArrStrSongList;
        gint iPlayIndex;
        GrassmoorVolumeType volume;
        GrassmoorMediaInfoType info;
        GrassmoorQueryOrderType queryOrder;
        gint SongListLen;
        gint inAlbumListLen;
        gint inArtistListLen;

        gboolean bActive;
        gboolean bUrlPlaying;
        gboolean seek;
};



/***********************************************************************************
        @Global variables
************************************************************************************/

/* Fi Object */
FramptonAgent* pFramptonAgentObject = NULL;
/* Gets internal the data for the relevent client */
FramptonAgentInternalData* audio_agent_get_client_struct(const gchar *appName);

GrassmoorTracker *tracker = NULL;

/***********************************************************************************
        @internally Linked variable
************************************************************************************/
/* Audio Service proxy object */
static TinwellPlayer *proxyPlayer = NULL;
/* Audio Service proxy object */
static PrestwoodService *proxyMedia = NULL;
/* Status bar Proxy */
MildenhallStatusbar *status_bar_proxy;
/* Agent service id */
static guint uinOwnerId = 0;
/* Hash table { string: application ID => FramptonAgentInternalData }
 * representing each client that is using the Frampton agent. */
static GHashTable* audioAgentAppHash = NULL;
/* save the client data permanetly in the file */
static SeatonPreference *sqliteObj = NULL;
/* last user name mode from app manager */
static gboolean gLastUserMode = FALSE;
/* active appName */
static gchar * pActiveClient = NULL;

/***********************************************************************************
        @Local type Definitions
************************************************************************************/
/* freeing the hash data */
static void audio_agent_hash_deinit (void);
/* registration acknowledgment from Audio service */
static void registration_info_message_cb(GObject *source_object, GAsyncResult *res, gpointer user_data);
/* unregistration acknowledgment from Audio service */
static void unregistration_info_message_cb(GObject *source_object, GAsyncResult *res, gpointer user_data);
/* Frees all the track related list */
static void audio_agent_free_player_tracks_list (FramptonAgentInternalData *clientAppData);
static void audio_agent_free_player_artist_tracks_list (FramptonAgentInternalData *clientAppData);
static void audio_agent_free_player_album_tracks_list (FramptonAgentInternalData *clientAppData);

/* Initialize , read and store the data from the database */
static void InitReadPdi(void);
/* Initialize , read and store the data from the database */
static void seek_info_message_cb( GObject *source_object, GAsyncResult *res, gpointer user_data);
/* Initialize , read and store the data from the database */
static void audio_agent_free_tracker_list (FramptonAgentInternalData *clientAppData);

/*********************************************************************************************
 * Function:    check_duplicate_entry
 * Description: check duplicte thumbs in case of artist/album mode
 * Parameters:  gconstpointer, gconstpointer
 * Return:      gint
 ********************************************************************************************/
static gint check_duplicate_entry (gconstpointer a, gconstpointer b)
{
        if(NULL != a && NULL != b && (0 == g_strcmp0( (gchar *)a, (gchar *)b)))
                return 0;
        else
                return -1;
}
	
/*********************************************************************************************
 * Function:    update_artist_url_list
 * Description: update url based on artist once at the begining
 * Parameters:  FramptonAgentInternalData*
 * Return:      gchar **
 ********************************************************************************************/
static gchar **update_artist_url_list(FramptonAgentInternalData* clientAppData)
{
	static gchar **list = NULL;
	GPtrArray *pArtistList;

	if(clientAppData->pArrStrArtistList)
		return clientAppData->pArrStrArtistList;

	//GPtrArray *pAlbumList = grassmoor_tracker_get_meta_list(clientAppData->volume, (gchar *)clientAppData->volumeMountPoint, GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM, 0, 1000, error);
	/* FIXME: Add error handling */
	pArtistList = grassmoor_tracker_get_url_list (tracker, clientAppData->volume,
		(gchar *) clientAppData->volumeMountPoint,
		GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO,
		GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST,
		clientAppData->queryOrder,
		0,
		1000,
		NULL);

	audio_agent_free_player_artist_tracks_list(clientAppData);
	if(pArtistList)
	{
		GList *pList = NULL;
		GList *pCompareList = NULL;
		guint i;

		for(i = 0; i < pArtistList->len; i++)
		{
			const char *str = g_ptr_array_index(pArtistList, i);
			/* FIXME: Add error handling */
			GrassmoorMediaInfo *pMediaInfo = grassmoor_tracker_get_media_file_info(tracker, str, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);

                        if (pMediaInfo && grassmoor_media_info_get_artist (pMediaInfo))
                        {
                                if(NULL !=  pCompareList && NULL != g_list_find_custom(pCompareList, grassmoor_media_info_get_artist (pMediaInfo), (GCompareFunc)check_duplicate_entry))
                                {
                                        grassmoor_media_info_free (pMediaInfo);
                                        continue;
                                }
                                else
                                {
                                        pCompareList = g_list_append (pCompareList, g_strdup (grassmoor_media_info_get_artist (pMediaInfo)));
                                        pList = g_list_append(pList, g_strdup(str));
                                        grassmoor_media_info_free (pMediaInfo);
                                }
                        }
                }
		list = g_new0(gchar *, g_list_length(pList) + 1);
                for(i = 0; i < g_list_length(pList); i++)
                {
                        list[i] = g_strdup(g_list_nth_data(pList, i));
                }
                list[i] = NULL;
                clientAppData->pArrStrArtistList = list;
                clientAppData->inArtistListLen = g_list_length(pList);

                g_ptr_array_free(pArtistList, TRUE);
                g_list_free_full (pList, g_free);
                g_list_free_full (pCompareList, g_free);
        }
        else
                clientAppData->inArtistListLen = 0;

        return list;
}

/*********************************************************************************************
 * Function:    update_album_url_list
 * Description: update url based on album once at the begining
 * Parameters:  FramptonAgentInternalData*
 * Return:      gchar **
 ********************************************************************************************/
static gchar **update_album_url_list(FramptonAgentInternalData* clientAppData)
{
	static gchar **list = NULL;
	GPtrArray *pAlbumList;

	if(clientAppData->pArrStrAlbumList)
		return clientAppData->pArrStrAlbumList;

	//GPtrArray *pAlbumList = grassmoor_tracker_get_meta_list(clientAppData->volume, (gchar *)clientAppData->volumeMountPoint, GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM, 0, 1000, error);
	/* FIXME: Add error handling */
	pAlbumList = grassmoor_tracker_get_url_list (tracker, clientAppData->volume,
		(gchar *) clientAppData->volumeMountPoint,
		GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO,
		GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM,
		clientAppData->queryOrder,
		0,
		1000,
		NULL);

	audio_agent_free_player_album_tracks_list(clientAppData);
	if(pAlbumList)
	{
		GList *pList = NULL;
		GList *pCompareList = NULL;
		guint i;

		for(i = 0; i < pAlbumList->len; i++)
		{
			const gchar *str = g_ptr_array_index (pAlbumList, i);
			/* FIXME: Add error handling */
			GrassmoorMediaInfo *pMediaInfo = grassmoor_tracker_get_media_file_info (tracker, str, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);

			if (pMediaInfo && grassmoor_media_info_get_album (pMediaInfo))
			{
				if(pCompareList && g_list_find_custom (pCompareList, grassmoor_media_info_get_album (pMediaInfo), (GCompareFunc)check_duplicate_entry))
				{
					grassmoor_media_info_free (pMediaInfo);
					continue;
				}
				else
				{
					pCompareList = g_list_append (pCompareList, g_strdup (grassmoor_media_info_get_album (pMediaInfo)));
					pList = g_list_append (pList, g_strdup (str));
					grassmoor_media_info_free (pMediaInfo);
				}
			}
		}
		list = g_new0(gchar *, g_list_length(pList) + 1);
		for(i = 0; i < g_list_length(pList); i++)
		{
			list[i] = g_strdup(g_list_nth_data(pList, i));
		}
		list[i] = NULL;
		clientAppData->pArrStrAlbumList = list;
		clientAppData->inAlbumListLen = g_list_length(pList);

		g_ptr_array_free(pAlbumList, TRUE);
		g_list_free_full (pList, g_free);
		g_list_free_full (pCompareList, g_free);
	}
	else
		clientAppData->inAlbumListLen = 0;

	return list;
}


/*****************************************************************************
 * queryMetatracker			:
 * @clientAppData           		: clientdata
 *
 * queries for track list from metatracker
 *****************************************************************************/
static gchar **queryMetatracker (FramptonAgentInternalData* clientAppData)
{
	static gchar **list = NULL;
	/* gets the url list between offset and limit value */
	//GPtrArray *pUrlList = grassmoor_tracker_get_url_list(GRASSMOOR_TRACKER_VOLUME_LOCAL, NULL, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, GRASSMOOR_TRACKER_MEDIA_INFO_URL, 0, 1000, NULL);
	/* FIXME: Add error handling */
	GPtrArray *pUrlList = grassmoor_tracker_get_url_list (tracker, clientAppData->volume,(gchar *)clientAppData->volumeMountPoint,GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO,clientAppData->info,clientAppData->queryOrder,0,1000, NULL);
	//GPtrArray *pUrlList = grassmoor_tracker_get_media_info_list(clientAppData->volume,(gchar *)clientAppData->volumeMountPoint,GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO,clientAppData->info,clientAppData->queryOrder,0,1000,error);
	/* Stored url list for further reference */
	audio_agent_free_player_tracks_list(clientAppData);
	clientAppData->songsList = pUrlList;
	if(pUrlList)
	{
		guint i;

		/* converting GPtrArray to gchar ** */
		list = g_new0(gchar *,pUrlList->len + 1);
		DEBUG ("Url list length = %d",pUrlList->len);
		for(i = 0; i < pUrlList->len; i++)
                {
                        //GrassmoorMediaInfo* MedStruct = g_ptr_array_index(pUrlList, i);
			const gchar *str = g_ptr_array_index(pUrlList,i);
                        list[i] = g_strdup (str);
                }

# if 0
		for(i = 0; i < pUrlList->len; i++)
		{
			GrassmoorMediaInfo* MedStruct = g_ptr_array_index(pUrlList, i);
			list[i] = g_strdup((gchar *)MedStruct->sUrl->str);
		}
# endif
		list[i] = NULL;
		clientAppData->pArrStrSongList = list;
		clientAppData->SongListLen = pUrlList->len;
		audio_agent_free_tracker_list(clientAppData);
	}
	else
	{
		/* if url list is NUL, assign 0 */
		clientAppData->SongListLen = 0;
		return NULL;
	}
	/* send url List as ouput of the method */
	return list;
}
/****************************************************************************
 * b_handle_get_play_list_clb	:
 * @invocation           	: Commmand line Argument Count
 * @arg_app_nam            	: App Nme
 * @arg_volume            	:
 * @arg_volume_mount_point      :
 * @arg_sort_type            	: Sorting order of list
 * @arg_Info_type            	:
 *
 * Call back when method called by the client
 ***************************************************************************/
static gboolean b_handle_get_play_list_clb (
		FramptonAgent *object,
		GDBusMethodInvocation *invocation,
		const gchar *arg_app_name,
		const gchar *arg_volume_mount_point,
		gint arg_sort_type,
		gint arg_Info_type)
{
	if(NULL != arg_app_name)
	{
		FramptonAgentInternalData *clientAppData;

		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		clientAppData = audio_agent_get_client_struct (arg_app_name);

		if(NULL != clientAppData)
		{
			gboolean is_mounted = FALSE;
			gchar **list;

		/* check if urlList is null, only when list is empty get the URL from metatracker */
			if(NULL != clientAppData->pArrStrSongList)
			{
				if (arg_Info_type == (gint) clientAppData->info
					&& arg_sort_type == (gint) clientAppData->queryOrder)
				{
					if((NULL == clientAppData->volumeMountPoint
								&& (g_strrstr(arg_volume_mount_point,"file:///") == NULL))
							|| ((NULL != clientAppData->volumeMountPoint)
								&& (g_strcmp0(clientAppData->volumeMountPoint,arg_volume_mount_point) == 0)))
					{
						DEBUG (" No query required");
						if(clientAppData->info == GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST)
							frampton_agent_complete_get_track_list(object,invocation,(const gchar *const *)clientAppData->pArrStrSongList, clientAppData->SongListLen, (const gchar *const *)clientAppData->pArrStrArtistList, clientAppData->inArtistListLen);
						else if (clientAppData->info == GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM)
							frampton_agent_complete_get_track_list(object,invocation,(const gchar *const *)clientAppData->pArrStrSongList, clientAppData->SongListLen, (const gchar *const *)clientAppData->pArrStrAlbumList, clientAppData->inAlbumListLen);
						else
							frampton_agent_complete_get_track_list(object,invocation,(const gchar *const *)clientAppData->pArrStrSongList, clientAppData->SongListLen, (const gchar *const *)clientAppData->pArrStrSongList, clientAppData->SongListLen);

						return TRUE;
					}
				}
			}
			/* Inputs for metatrcaker */
			clientAppData->info = arg_Info_type;
			clientAppData->queryOrder = arg_sort_type;
			/* check if the mount path is correct */
			if(g_str_has_prefix(arg_volume_mount_point,"file:///"))
			{
				DEBUG (" Mount check");
				prestwood_service_call_is_mounted_sync (
						proxyMedia,
						(const gchar *)arg_volume_mount_point,
						&is_mounted,
						NULL,
						NULL);
				if(is_mounted)
				{
					DEBUG (" removable volume mounted");
					clientAppData->volume = FRAMPTON_AGENT_VOLUME_REMOVABLE;
					clientAppData->volumeMountPoint = g_strdup (arg_volume_mount_point);
				}
				else
				{
						DEBUG (" local volume mounted");
					clientAppData->volume = FRAMPTON_AGENT_VOLUME_LOCAL;
					clientAppData->volumeMountPoint = NULL;
				}
			}
			/* else fetch the local media */
			else
			{
				DEBUG (" local volume mounted");
				clientAppData->volume = FRAMPTON_AGENT_VOLUME_LOCAL;
				clientAppData->volumeMountPoint = NULL;
			}
			/* query from meta tracker */
			list = queryMetatracker(clientAppData);
			update_album_url_list(clientAppData);
                        update_artist_url_list(clientAppData);
			if(clientAppData->info == GRASSMOOR_TRACKER_MEDIA_INFO_ARTIST)
				frampton_agent_complete_get_track_list(object,invocation,(const gchar *const *)list,clientAppData->SongListLen, (const gchar *const *)clientAppData->pArrStrArtistList, clientAppData->inArtistListLen);
			else if (clientAppData->info == GRASSMOOR_TRACKER_MEDIA_INFO_ALBUM)
				frampton_agent_complete_get_track_list(object,invocation,(const gchar *const *)list,clientAppData->SongListLen, (const gchar *const *)clientAppData->pArrStrAlbumList, clientAppData->inAlbumListLen);
			else
				frampton_agent_complete_get_track_list(object,invocation,(const gchar *const *)list,clientAppData->SongListLen, (const gchar *const *)list,clientAppData->SongListLen);
		}
		else
		{
			g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "Client not registered");
		}
	}
	else
	{
		g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "Invalid app name");
	}

	return TRUE;
}
/****************************************************************************
 * b_handle_get_player_state_clb	:
 * @invocation           		: Commmand line Argument Count
 * @arg_app_name            		: App Name which called the method
 *
 * callback called upon calling the get_player_state method
 ***************************************************************************/
static gboolean b_handle_get_player_state_clb(FramptonAgent *object,
				    GDBusMethodInvocation *invocation,
				    const gchar *arg_app_name)
{
	if (arg_app_name)
	{
		FramptonAgentInternalData *clientAppData;
		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		clientAppData = audio_agent_get_client_struct (arg_app_name);
		if(clientAppData != NULL)
		{
			/* send the client data */
			/* if clientAppData->curPlayingTrack is NULL, sending empty string */
			if((gLastUserMode == FALSE) || (NULL == clientAppData->curPlayingTrack))
				frampton_agent_complete_get_player_state(object,invocation,clientAppData->bShuffle,
						clientAppData->bRepeat,"",clientAppData->trackPos,
						clientAppData->info,clientAppData->queryOrder,FRAMPTON_AGENT_NONE);
			else
				frampton_agent_complete_get_player_state(object,invocation,clientAppData->bShuffle,
						clientAppData->bRepeat,clientAppData->curPlayingTrack,clientAppData->trackPos,
						clientAppData->info,clientAppData->queryOrder,clientAppData->playState);
		}
	}
	else
	{

		g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "Invalid app name");
	}
	return TRUE;
}

/****************************************************************************
 * pause_info_message_cb 	:
 * @source_object           :
 * @res            :
 * @user_data            :
 *
 * acknowlegde from Audio service for pause method
 ***************************************************************************/
static void pause_info_message_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
	gboolean regState;
	GError *error = NULL;
	/* collect state to know the method success */
	regState =  tinwell_player_call_pause_playback_finish(proxyPlayer, res ,&error);
	if(regState)
	{
#if 0
		FramptonAgentInternalData *clientData = user_data;
		if(( clientData->trackPos > 0.0L) && ( clientData->trackPos <= 1.0L))
                        {

                                tinwell_player_call_seek_track(proxyPlayer,clientData->appName,clientData->trackPos,NULL,seek_info_message_cb,NULL);

                                DEBUG ("play method success");
                        }
#endif
		DEBUG ("pause method success");

	}
	else
	{
		DEBUG ("pause method failed");
	}
}
/****************************************************************************
 * b_handle_pause_playback_clb
 * @object           : Commmand line Argument Count
 * @invocation            : Array of strings
 * @arg_app_name            : Array of strings
 *
 * callback called upon calling the pause-playback method
 ***************************************************************************/
static gboolean b_handle_pause_playback_clb (FramptonAgent *object,
			    GDBusMethodInvocation *invocation,
			    const gchar *arg_app_name)
{
	if(NULL != arg_app_name)
	{
		tinwell_player_call_pause_playback(proxyPlayer, arg_app_name, NULL, pause_info_message_cb, NULL);

		frampton_agent_complete_pause_playback(object,invocation);

	}
	else
	{
		g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "Invalid app name");
	}
	return TRUE;
}
/****************************************************************************
 * audio_agent_get_client_struct	:
 * @appName            : name of the app
 *
 * reading perticular client data ffrom hash DS
 ***************************************************************************/
FramptonAgentInternalData* audio_agent_get_client_struct(const gchar *appName)
{
        FramptonAgentInternalData *clientAppData = NULL;
	gchar *newAppname = NULL;
        if(NULL != audioAgentAppHash)
        {
		/* If client is Agent widget, then it has to refer to main app/client data */
		if(g_str_has_suffix(appName,AGENT_WIDGET_NAME))
			newAppname = g_strndup(appName,strlen(appName) - strlen(AGENT_WIDGET_NAME));
		else
			newAppname = g_strdup(appName);

                /* Add the new entry into the hash table */
                clientAppData = g_hash_table_lookup(audioAgentAppHash, (gpointer) newAppname);

		/* Free App name */
		if(newAppname)
		{
			g_free(newAppname);
			newAppname = NULL;
		}

        }

        return clientAppData;
}

/****************************************************************************
 * audio_agent_create_client_struct	: private
 * @appName            			: name of the app
 *
 * Mem allocation for client data
 ***************************************************************************/
static FramptonAgentInternalData* audio_agent_create_client_struct(const gchar *appName)
{
        FramptonAgentInternalData *clientAppData = g_new0(FramptonAgentInternalData, 1);
        /* Assign the parameters that are to be treated as default */
        if(NULL != clientAppData)
        {
		/* serach for appname in pdi and get the data when
		it got registered previous time */

                clientAppData->appName = g_strdup(appName);
                clientAppData->curPlayingTrack = NULL;
		clientAppData->pIconPath = NULL;
                clientAppData->bRepeat = FALSE;
                clientAppData->bShuffle = FALSE;
                clientAppData->songsList = NULL;
                clientAppData->trackPos = 0.0;
                clientAppData->playState = FRAMPTON_AGENT_NONE;
		clientAppData->volumeMountPoint = NULL;
		clientAppData->pArrStrSongList = NULL;
		clientAppData->seek = FALSE;

        }

        return clientAppData;
}
/****************************************************************************
 * audio_agent_add_client_to_hash	: private
 *
 * After registration, creating new client data and adding to hash
 ***************************************************************************/
static void audio_agent_add_client_to_hash(const gchar *appName,
				FramptonAgentInternalData *clientAppData)
{
        if(NULL == appName || NULL == clientAppData)
                return;

        /* Add the new entry into the hash table */
        /* FIXME: this almost certainly leaks memory, since the hash table
         * doesn't have free-functions configured */
        g_hash_table_replace (audioAgentAppHash,
            g_strdup (appName), clientAppData);
}

/****************************************************************************
 * b_handle_register_audio_agent_clb	:
 *
 * Registering App to agent service
 ***************************************************************************/
static gboolean b_handle_register_audio_agent_clb (FramptonAgent *object,
			    GDBusMethodInvocation *invocation,
		   	    const gchar *arg_app_name,
			    const gchar *icon_path)
{
	DEBUG ("Registration requested by %s", arg_app_name);
	if(NULL != arg_app_name)
	{
		FramptonAgentInternalData *clientAppData;

		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		clientAppData = audio_agent_get_client_struct (arg_app_name);
		if(NULL == clientAppData)
		{
			clientAppData = audio_agent_create_client_struct(arg_app_name);
			if(NULL != clientAppData)
			{
				/* Store icon path for further use */
				clientAppData->pIconPath = g_strdup (icon_path);
				/* Add new client details to service hash list */
                                audio_agent_add_client_to_hash(arg_app_name, clientAppData);
				 /* Registration Success */
                                frampton_agent_complete_register(object, invocation, TRUE);
			}
			else
			{
                                g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "Cannot register client");

                                /* Registration Failure */
                                frampton_agent_complete_register(object, invocation, FALSE);
                        }

		}
		else
		{
			/* Already registered. No duplicate registrations */
			frampton_agent_complete_register(object, invocation, TRUE);
		}

	}
	else
	{

		g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "Invalid app name");

		/* Registration Failure */
		frampton_agent_complete_register(object, invocation, FALSE);
	}
	/* Every app which will register to agent will also register to audio service */
	tinwell_player_call_register_player(proxyPlayer, arg_app_name, NULL, registration_info_message_cb, NULL);
	return TRUE;
}
/****************************************************************************
 * resume_info_message_cb :
 *
 * Acknowlegement callback on resuming the audio from audio service
 ***************************************************************************/
static void resume_info_message_cb(GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
	gboolean regState;
	GError *error = NULL;

	regState =  tinwell_player_call_resume_playback_finish(proxyPlayer, res ,&error);
	if(regState)
	{
		DEBUG ("resume method success");

	}
	else
	{
		WARNING ("resume method failed with error: %s", error->message);
		g_error_free (error);
	}
}

/****************************************************************************
 * b_handle_resume_playback_clb	:
 *
 * method callback to resume audio, requested from client
 ***************************************************************************/
static gboolean b_handle_resume_playback_clb (FramptonAgent *object,
		    GDBusMethodInvocation *invocation,
		    const gchar *arg_app_name)

{
	if(NULL != arg_app_name)
	{
		tinwell_player_call_resume_playback(proxyPlayer, arg_app_name,NULL,resume_info_message_cb, NULL);

		frampton_agent_complete_resume_playback(object,invocation);
	}
	else
	{
		g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "Invalid app name");
	}

	return TRUE;
}

/****************************************************************************
 * seek_info_message_cb	:
 *
 * Acknowlegement callback on seeking a position the audio from audio service
 ***************************************************************************/
static void seek_info_message_cb( GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
        gboolean regState;
        GError *error = NULL;

        tinwell_player_call_seek_track_finish(proxyPlayer, &regState,res , &error);
        if(regState)
        {
                DEBUG ("seek method success ");

        }
        else
        {
                WARNING ("seek method failed with error: %s", error->message);
                g_error_free (error);
        }
}

/****************************************************************************
 * b_handle_seek_track_clb	:
 *
 * method callback for seek request from client
 ***************************************************************************/
static gboolean b_handle_seek_track_clb (FramptonAgent *object,
			GDBusMethodInvocation *invocation,
			const gchar *arg_app_name,
			gdouble arg_seek_pos)
{
        if(NULL != arg_app_name)
	{
		if((arg_seek_pos >= 0.0L) && (arg_seek_pos <= 1.0L))
		{
			tinwell_player_call_seek_track(proxyPlayer,arg_app_name,arg_seek_pos,NULL,seek_info_message_cb,NULL);

			frampton_agent_complete_track_seek(object,invocation);
		}
	}
        else
        {
                g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "Invalid app name");
        }

	return TRUE;
}

/****************************************************************************
 * set_repeat_state_cb	:
 *
 * Acknowlegement callback for setting the falg  for repeat from audio service
 ***************************************************************************/
static void set_repeat_state_cb( GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
        gboolean regState;
        GError *error = NULL;

        regState = tinwell_player_call_set_repeat_state_finish(proxyPlayer, res , &error);
        if(regState)
        {
                DEBUG ("set-repeat method success");

        }
        else
        {
                DEBUG ("set-repeat method failed with error: %s", error->message);
                g_error_free (error);
        }
}

/****************************************************************************
 * b_handle_set_repeat_state_clb	:
 *
 * method callback for seek request from client
 ***************************************************************************/
static gboolean b_handle_set_repeat_state_clb ( FramptonAgent *object,
		GDBusMethodInvocation *invocation,
		const gchar *arg_app_name,
		gboolean arg_rep_state)
{
	if(NULL != arg_app_name)
	{
		FramptonAgentInternalData *clientAppData;

		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		clientAppData = audio_agent_get_client_struct (arg_app_name);
		if(NULL != clientAppData)
		{
			clientAppData->bRepeat = arg_rep_state;
			tinwell_player_call_set_repeat_state(proxyPlayer,arg_app_name,arg_rep_state,NULL,set_repeat_state_cb,NULL);
			frampton_agent_complete_set_repeat_state(object,invocation);
		}
		else
		{
			WARNING ("App %s is not registered", arg_app_name);
		}
	}
	else
	{
                g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "Invalid app name");
	}
	return TRUE;
}

/****************************************************************************
 * set_shuffle_state_cb:
 *
 * Acknowlegement callback on setting the shuffle flag  from audio service
 *
 ***************************************************************************/
static void set_shuffle_state_cb ( GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
        gboolean regState;
        GError *error = NULL;

        regState = tinwell_player_call_set_shuffle_state_finish(proxyPlayer, res ,&error);
        if(regState)
        {
                DEBUG ("set-shuffle method success");

        }
        else
        {
                WARNING ("set-shuffle method failed with error: %s", error->message);
                g_error_free (error);
        }
}

/****************************************************************************
 * b_handle_set_shuffle_state_clb	:
 *
 * method callback for shuffle request from client
 ***************************************************************************/
static gboolean b_handle_set_shuffle_state_clb (FramptonAgent *object,
	    GDBusMethodInvocation *invocation,
    	    const gchar *arg_app_name,
    	    gboolean arg_shuf_state)
{
	if(NULL != arg_app_name)
	{
		FramptonAgentInternalData *clientAppData;

		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		clientAppData = audio_agent_get_client_struct (arg_app_name);
		if(NULL != clientAppData)
		{
			clientAppData->bShuffle = arg_shuf_state;
			tinwell_player_call_set_shuffle_state(proxyPlayer,arg_app_name,arg_shuf_state,NULL,set_shuffle_state_cb,NULL);
			frampton_agent_complete_set_shuffle_state(object,invocation);
		}
		else
		{
			g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "App is not registered");
		}
	}
	else
	{
                g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "App Name cannot be NULL");
	}
	return TRUE;
}
#if 0
gboolean
pause_cb (gpointer userData)
{
	FramptonAgentInternalData *ClientData = userData;
	if( ClientData->playState == FRAMPTON_AGENT_PLAYING)
		tinwell_player_call_pause_playback(proxyPlayer, ClientData->appName, NULL, pause_info_message_cb, ClientData);
	return FALSE;
}
#endif

/****************************************************************************
 * play_message_info_cb	:
 * @argc           : Commmand line Argument Count
 * @argv            : Array of strings
 *
 * method callback for seek request from client
 ***************************************************************************/
static void play_message_info_cb ( GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
        gboolean regState;
        GError *error = NULL;
        //gchar *appName = g_strdup (user_data);

        regState = tinwell_player_call_start_playback_finish(proxyPlayer, res ,&error);
        if(regState)
        {
#if 0
		if(appName)
		{
			FramptonAgentInternalData *clientData = audio_agent_get_client_struct(appName);

			if(clientData)
			{

#if 1
				if((FALSE == clientData->bUrlPlaying) && ((clientData->trackPos > 0.0L) && ( clientData->trackPos <= 1.0L)))
				{
					//g_timeout_add(2000,pause_cb,clientData);
					tinwell_player_call_seek_track(proxyPlayer,clientData->appName,clientData->trackPos,NULL,seek_info_message_cb,NULL);
					DEBUG ("play successful for %s and current playing track is %s", clientData->appName, clientData->curPlayingTrack);
				}
			}
#endif

		}
#endif
	}
        else
        {
               WARNING ("play method failed with error: %s", error->message);
               g_error_free (error);
        }
}

/****************************************************************************
 * b_handle_start_playback_clb	:
 * @argc           : Commmand line Argument Count
 * @argv            : Array of strings
 *
 * method callback for seek request from client
 ***************************************************************************/
static gboolean b_handle_start_playback_clb (FramptonAgent *object,
    GDBusMethodInvocation *invocation,
    const gchar *arg_app_name,
    const gchar *arg_track_name)
{
	if(NULL != arg_app_name)
	{
		FramptonAgentInternalData *clientAppData;

		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		clientAppData = audio_agent_get_client_struct (arg_app_name);
		if(NULL != clientAppData)
		{
			if(arg_track_name)
			{
				gint i;
				gboolean bFound = FALSE;
				for(i = 0; clientAppData->pArrStrSongList[i]; i++)
				{
					if(g_strcmp0(clientAppData->pArrStrSongList[i],arg_track_name) == 0)
					{
						bFound = TRUE;
						break;
					}
				}
				if(bFound)
				{
					if(clientAppData->curPlayingTrack)
					{
						g_free(clientAppData->curPlayingTrack);
						clientAppData->curPlayingTrack = NULL;
					}
					clientAppData->curPlayingTrack = g_strdup(arg_track_name);
					clientAppData->iPlayIndex = i;

					DEBUG (" play request to audio service  %s from %s to play %s",clientAppData->curPlayingTrack,arg_app_name,arg_track_name);
					tinwell_player_call_start_playback(proxyPlayer,arg_app_name,arg_track_name,0 , NULL, play_message_info_cb, NULL);
					frampton_agent_complete_start_playback(object,invocation);
				}
				else
				{
					DEBUG (" %s URL is not in the list", arg_track_name);
				}

			}
			else
			{
				DEBUG ("invalid track");
			}
		}
		else
		{
			g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "App is not registered");
		}
	}
	else
	{
		g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "App is not registered");
	}
	return TRUE;
}

/****************************************************************************
 * b_handle_track_list_order_changed_clb	:
 *
 * callback calld when client calls the method 
 ***************************************************************************/
static gboolean b_handle_track_list_order_changed_clb (FramptonAgent *object,
			    GDBusMethodInvocation *invocation,
			    const gchar *arg_app_name,
			    gint arg_query_type)
{
	if(arg_app_name)
	{
		FramptonAgentInternalData *clientAppData = audio_agent_get_client_struct(arg_app_name);//audio_agent_create_client_struct(arg_app_name);
		if(clientAppData)
		{
			if (arg_query_type == (int) clientAppData->queryOrder)
				return TRUE;
			clientAppData->queryOrder = arg_query_type;
			/* query from meta tracker update the song list*/
			queryMetatracker(clientAppData);
		}
		else
		{
			g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "App is not registered");
		}
	}
	else
	{
                g_dbus_method_invocation_return_dbus_error (invocation, "Frampton.Agent.Error.Failed", "App name cannaot be NULL");
	}
	return TRUE;
}
/****************************************************************************
 * b_handle_track_list_order_changed_clb	:
 *
 * callback calld when client calls the method 
 ***************************************************************************/
static gboolean b_handle_play_url(
	    FramptonAgent *object,
	    GDBusMethodInvocation *invocation,
	    const gchar *arg_app_name,
	    gchar *arg_url)
{
	DEBUG ("App %s requested to play url %s", arg_app_name, arg_url);
	if(arg_app_name)
	{
		FramptonAgentInternalData *clientAppData = audio_agent_get_client_struct(arg_app_name);//audio_agent_create_client_struct(arg_app_name);
		if(clientAppData)
		{
			if(NULL != arg_url)
			{
				clientAppData->bUrlPlaying = TRUE;
				tinwell_player_call_start_playback(proxyPlayer,arg_app_name,arg_url,0 , NULL, play_message_info_cb, NULL);
				//tinwell_player_call_pause_playback(proxyPlayer, clientAppData->appName, NULL, pause_info_message_cb, NULL);
			}

		}
	}
	return TRUE;	
}

/****************************************************************************
 * Function	: connect_signal_handler_for_methods
 * @argc           : Commmand line Argument Count
 * @argv            : Array of strings
 *
 * Entry point of main thread.
 ***************************************************************************/
static void connect_signal_handler_for_methods (FramptonAgent *self)
{
  /* provide signal handlers for all methods */
        g_signal_connect (self,
                    "handle-get-track-list",
                    G_CALLBACK (b_handle_get_play_list_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-get-player-state",
                    G_CALLBACK (b_handle_get_player_state_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-pause-playback",
                    G_CALLBACK (b_handle_pause_playback_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-register",
                    G_CALLBACK (b_handle_register_audio_agent_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-resume-playback",
                    G_CALLBACK (b_handle_resume_playback_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-track-seek",
                    G_CALLBACK (b_handle_seek_track_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-set-repeat-state",
                    G_CALLBACK (b_handle_set_repeat_state_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-set-shuffle-state",
                    G_CALLBACK (b_handle_set_shuffle_state_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-start-playback",
                    G_CALLBACK (b_handle_start_playback_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-track-list-order-changed",
                    G_CALLBACK (b_handle_track_list_order_changed_clb),
                    NULL);
        g_signal_connect (self,
                    "handle-play-url",
                    G_CALLBACK (b_handle_play_url),
                    NULL);
}

static void unregistration_info_message_cb(GObject *source_object,
				 GAsyncResult *res,
				 gpointer user_data)
{
	gboolean regState;

	tinwell_player_call_unregister_player_finish(proxyPlayer, &regState, res , user_data);

	if(TRUE == regState)
		DEBUG (" unregistered successfully %s ",(gchar *)user_data);
}

/**************************************************************
 * unregister_all_app	:
 *
 * On exiting clear all the mem held by this process as well
 * unregister all the apps registered to Audio service
 *************************************************************/
static void unregister_all_app(void)
{
	guint uApps = g_hash_table_size(audioAgentAppHash);
	GList *keys = g_hash_table_get_keys(audioAgentAppHash);
	guint i;

	for(i = 0; i < uApps ;i++)
	{
		gchar *appName = g_list_nth_data(keys,i);
		tinwell_player_call_unregister_player(proxyPlayer,appName, NULL, unregistration_info_message_cb, appName);
	}
}

static void write_to_pdi(void)
{
	if(audioAgentAppHash)
	{
		GList *keys = g_hash_table_get_keys (audioAgentAppHash);
		gint len = g_list_length (keys);
		gint i;

		/* check if the device is disconnected */

		for (i = 0; i<len; i++)
		{
			FramptonAgentInternalData *clientAppData;
			gchar *str = g_list_nth_data (keys, i);
			GHashTable *hash;
			GPtrArray *gpArray;
			int inResult;

			clientAppData = g_hash_table_lookup (audioAgentAppHash, str);

			/* before removing the data from the hash table
			   store it in the PDI TBD*/

			hash = g_hash_table_new_full(g_str_hash, g_str_equal,g_free,g_free);
			g_hash_table_insert(hash, g_strdup("appName"), g_strdup(clientAppData->appName));
			g_hash_table_insert(hash, g_strdup("track"), g_strdup(clientAppData->curPlayingTrack));//g_strdup_printf(("%02d:%02d min"),(int) voicenotes->priv->timer / 60, (int) voicenotes->priv->timer % 60));
			g_hash_table_insert(hash, g_strdup("shuffle"), g_strdup_printf("%d",clientAppData->bShuffle));//g_strdup_printf(("%02d:%02d min"),(int) voicenotes->priv->timer / 60, (int) voicenotes->priv->timer % 60));
			g_hash_table_insert(hash, g_strdup("repeat"),g_strdup_printf("%d",clientAppData->bRepeat));
			if(NULL != clientAppData->volumeMountPoint)
				g_hash_table_insert(hash, g_strdup("mountpath"),g_strdup(clientAppData->volumeMountPoint));
			if(pActiveClient)
			{
				if(g_strcmp0(pActiveClient,clientAppData->appName) == 0)
					g_hash_table_insert(hash, g_strdup("active"),g_strdup_printf("%d",1));
				else
					g_hash_table_insert(hash, g_strdup("active"),g_strdup_printf("%d",0));
			}
			else
				g_hash_table_insert(hash, g_strdup("active"),g_strdup_printf("%d",0));


			g_hash_table_insert(hash, g_strdup("position"),g_strdup_printf("%.3lf",clientAppData->trackPos));
			g_hash_table_insert(hash, g_strdup("volume"),g_strdup_printf("%d",clientAppData->volume));
			g_hash_table_insert(hash, g_strdup("info"),g_strdup_printf("%d",clientAppData->info));
			g_hash_table_insert(hash, g_strdup("queryOrder"),g_strdup_printf("%d",clientAppData->queryOrder));
			g_hash_table_insert(hash, g_strdup("state"),g_strdup_printf("%d",clientAppData->playState));
			g_hash_table_insert(hash, g_strdup("iconPath"),g_strdup(clientAppData->pIconPath));

			gpArray = seaton_preference_get (sqliteObj, NULL, "appName", clientAppData->appName);
			if(NULL != gpArray)
			{
				seaton_preference_remove(sqliteObj,NULL,"appName",clientAppData->appName);
			}
			inResult = seaton_preference_add_data (sqliteObj, NULL, hash);
			if( !inResult )
			{
				DEBUG ("Data Added to pdi");
			}
			else
			{
				WARNING ("Data Adding to pdi Failed");
			}
			g_hash_table_destroy(hash);
		}
	}
}

/**************************************************************
 * audio_client_unregister	:
 *
 * On exiting clear all the mem held by this process as well
 * unregister all the apps registered to Audio service
 *************************************************************/
static gboolean
audio_client_unregister (gpointer nil G_GNUC_UNUSED)
{
	/* Store it to data permanet into file and later clear the data */
	write_to_pdi();
	/* unregister all App which was registered to Audio services */
	unregister_all_app();

	/* destory hash table */
	audio_agent_hash_deinit();

	exit(0);

	/* Not actually reached */
	g_return_val_if_reached (G_SOURCE_CONTINUE);
}

static void play_completed_cb(TinwellPlayer *object,
			const gchar *arg_app_name,
			const gchar *arg_file_name)
{
	if(NULL != arg_app_name)
	{
		FramptonAgentInternalData *clientAppData;

		DEBUG (" App Name = %s, play completed for file = %s", arg_app_name,arg_file_name);
		/* emit play completed to client */
		frampton_agent_emit_play_completed(pFramptonAgentObject,arg_app_name,arg_file_name);
		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		clientAppData = audio_agent_get_client_struct (arg_app_name);
		if(NULL != clientAppData)
		{
			if(clientAppData->bUrlPlaying)
			{
				clientAppData->bUrlPlaying = FALSE;
				//if(FRAMPTON_AGENT_PAUSED == clientAppData->playState)
				{
					if((clientAppData->trackPos > 0.0L) && ( clientAppData->trackPos <= 1.0L))
					{
						DEBUG ("clientData->trackPos = %lf",clientAppData->trackPos);
						DEBUG ("%lf", clientAppData->trackPos);
						DEBUG ("%s", clientAppData->appName);

						//g_timeout_add(2000,pause_cb,clientData);
						//tinwell_player_call_seek_track(proxyPlayer,clientAppData->appName,clientAppData->trackPos,NULL,seek_info_message_cb,NULL);
						//tinwell_player_call_resume_playback(proxyPlayer, clientAppData->appName,NULL,resume_info_message_cb, NULL);
						if(clientAppData->pArrStrSongList != NULL)
						{
							gint i;
							gboolean bFound = FALSE;
							for(i = 0; clientAppData->pArrStrSongList[i]; i++)
							{
								if(g_strcmp0(clientAppData->pArrStrSongList[i],clientAppData->curPlayingTrack) == 0)
								{
									bFound = TRUE;
									break;
								}
							}
							if(bFound)
							{
								clientAppData->iPlayIndex = i;

								tinwell_player_call_start_playback(proxyPlayer,arg_app_name, clientAppData->curPlayingTrack ,0 , NULL, play_message_info_cb, (gpointer)g_strdup(arg_app_name));
							}
						}
					}

				}
			}
			else
			{
				if(clientAppData->pArrStrSongList != NULL)
				{
					gint32 index = clientAppData->iPlayIndex;
					/* if both are on give prio for repeat */
					if(clientAppData->bShuffle && clientAppData->bRepeat);
					/* if shuffle is on get the random index */
					else if(clientAppData->bShuffle)
					{
						/* If shuffle is selected, select a new song for shuffle */
						gint32 shuffleIndex = g_rand_int_range(g_rand_new(), 0, clientAppData->SongListLen);
						if(shuffleIndex != clientAppData->iPlayIndex)
							index = shuffleIndex;
						else
						{
							index = clientAppData->iPlayIndex + 1;
							if(index >= clientAppData->SongListLen)
								index = 0;
						}
					}
					/* for normal */
					else
					{
						if(TRUE != clientAppData->bRepeat)
							index =  clientAppData->iPlayIndex + 1;
						if(index >= clientAppData->SongListLen)
							index = 0;
					}

					/* if index is changed, then change in the client data */
					if(clientAppData->iPlayIndex != index )
					{
						gchar *url;

						clientAppData->iPlayIndex = index;
						if(clientAppData->curPlayingTrack)
						{
							g_free(clientAppData->curPlayingTrack);
							clientAppData->curPlayingTrack = NULL;
						}
						url = clientAppData->pArrStrSongList[clientAppData->iPlayIndex];
						if(url)
						{
							clientAppData->curPlayingTrack = g_strdup(url);
						}
					}
					/* start play back with the given url */
					DEBUG ("Song = %s",clientAppData->curPlayingTrack);
					tinwell_player_call_start_playback(proxyPlayer,arg_app_name, clientAppData->curPlayingTrack ,0 , NULL, play_message_info_cb, NULL);
				}
			}
		}
		else
		{
			DEBUG ("app is not registered to Agent Service");
		}
	}
}

/**************************************************************
 * set_status_bar_clb :
 * @argc           : Commmand line Argument Count
 * @argv            : Array of strings
 *
 *
 *************************************************************/
static void set_status_bar_clb( GObject *source_object,GAsyncResult *res,gpointer user_data)
{
    GError *error = NULL;

    mildenhall_statusbar_call_set_status_bar_info_finish(status_bar_proxy, res, &error);
}

static void play_started_cb(TinwellPlayer *object,
			const gchar *arg_app_name,
			const gchar *arg_file_name)
{
	if(NULL != arg_app_name)
	{
		/* Check if the client is already registered. If so just return.
		 * If not, register the client */
		FramptonAgentInternalData* clientAppData = audio_agent_get_client_struct(arg_app_name);
		if(NULL != clientAppData)
		{
			GrassmoorMediaInfo* MedStruct;

			if(FALSE == clientAppData->bUrlPlaying)
			{
				/* upadte the state to playing, for current playing track */
				clientAppData->playState = FRAMPTON_AGENT_PLAYING;
			}	
			
			/* update the active client */
			if(pActiveClient)
			{
				g_free(pActiveClient);
				pActiveClient = NULL;
			}
			gLastUserMode = TRUE;
			pActiveClient = g_strdup(arg_app_name);
			/* Emit signal::play-started to client */
			frampton_agent_emit_play_started(pFramptonAgentObject,arg_app_name,arg_file_name);
			/*  */
			/* FIXME: Add error handling */
			MedStruct = grassmoor_tracker_get_media_file_info (tracker, arg_file_name, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);

			if (MedStruct && grassmoor_media_info_get_title (MedStruct))
			{
				mildenhall_statusbar_call_set_status_bar_info (status_bar_proxy, clientAppData->appName, "NULL", clientAppData->pIconPath, g_strdup (grassmoor_media_info_get_title (MedStruct)), NULL, set_status_bar_clb, NULL);
			}
			else
			{
				/* FIXME: Add error handling */
				MedStruct = grassmoor_tracker_get_media_file_info (tracker, arg_file_name, GRASSMOOR_TRACKER_MEDIA_TYPE_UNKNOWN, NULL);
				if (MedStruct && grassmoor_media_info_get_title (MedStruct))
                       		{
                                       	mildenhall_statusbar_call_set_status_bar_info (status_bar_proxy, clientAppData->appName, "NULL", clientAppData->pIconPath, g_strdup (grassmoor_media_info_get_title (MedStruct)), NULL, set_status_bar_clb, NULL);
                                }
				else
					mildenhall_statusbar_call_set_status_bar_info (status_bar_proxy, clientAppData->appName, "NULL", clientAppData->pIconPath, g_strdup("Unknown"), NULL, set_status_bar_clb, NULL);
			}
			
			/* free media info data */
			grassmoor_media_info_free (MedStruct);
#if 0
			if(( clientAppData->trackPos > 0.0L) && ( clientAppData->trackPos <= 1.0L))
			{
				tinwell_player_call_seek_track(proxyPlayer,clientAppData->appName,clientAppData->trackPos,NULL,seek_info_message_cb,NULL);
			}
#endif
		}
		else
		{
			DEBUG ("App is not registered to Agent Service");
		}
	}
}

static void play_position_cb(TinwellPlayer *object,
		const gchar *arg_app_name,
		const gchar *arg_file_name,
		gdouble arg_track_pos)
{
	if(NULL != arg_app_name)
	{
		FramptonAgentInternalData *clientAppData;

		/* get Client data */
		clientAppData = audio_agent_get_client_struct (arg_app_name);
		if(NULL != clientAppData)
		{
			DEBUG (" play successful");

			if((TRUE == gLastUserMode) && (TRUE == clientAppData->seek) && ((clientAppData->trackPos > 0.0L) && ( clientAppData->trackPos <= 1.0L)))
			{
				DEBUG ("Playing track %s from track position %lf requested by app: %s", clientAppData->curPlayingTrack, clientAppData->trackPos, clientAppData->appName);
				//g_timeout_add(2000,pause_cb,clientData);
				tinwell_player_call_seek_track(proxyPlayer,clientAppData->appName,clientAppData->trackPos,NULL,seek_info_message_cb,NULL);

				clientAppData->seek = FALSE;
			}
			/* if current playing track is pointing same as the filename
			   only then emit the signal */
			if(g_strcmp0(clientAppData->curPlayingTrack,arg_file_name) == 0)
			{
				/* update the track position for current playing track*/
				clientAppData->trackPos = arg_track_pos;
				/* Emit signal::play-position to client */
				frampton_agent_emit_play_position(pFramptonAgentObject,arg_app_name,arg_file_name,arg_track_pos);
			}
			else if (clientAppData->bUrlPlaying == TRUE)
			{
				DEBUG ("arg_track_pos = %f",arg_track_pos);
				frampton_agent_emit_play_position(pFramptonAgentObject,arg_app_name,arg_file_name,arg_track_pos);
			}
			DEBUG ("clientAppData->bUrlPlaying = %d",clientAppData->bUrlPlaying);
		}

	}
}

static void play_resumed_cb(TinwellPlayer *object,
		const gchar *arg_app_name,
		const gchar *arg_file_name,
		gdouble arg_track_pos)
{
	if(NULL != arg_app_name)
	{
		/* get Client data */
		FramptonAgentInternalData* clientAppData = audio_agent_get_client_struct(arg_app_name);
		if(NULL != clientAppData)
		{
			/* if current playing track is pointing same as the filename
			   only then emit the signal */
			if(g_strcmp0(clientAppData->curPlayingTrack,arg_file_name) == 0)
			{
				GrassmoorMediaInfo *MedStruct;

				/* update state for current playing track */
				clientAppData->playState = FRAMPTON_AGENT_PLAYING;
				/* Emit signal::play-position to client */
				frampton_agent_emit_play_resumed(pFramptonAgentObject,arg_app_name,arg_file_name,arg_track_pos);
				/*  */
				/* FIXME: Add error handling */
				MedStruct = grassmoor_tracker_get_media_file_info (tracker, arg_file_name, GRASSMOOR_TRACKER_MEDIA_TYPE_AUDIO, NULL);
				if (MedStruct && grassmoor_media_info_get_title (MedStruct))
				{
					mildenhall_statusbar_call_set_status_bar_info (status_bar_proxy, clientAppData->appName, "NULL", clientAppData->pIconPath, g_strdup (grassmoor_media_info_get_title (MedStruct)), NULL, set_status_bar_clb, NULL);
				}
				else
				{
					/* FIXME: Add error handling */
					MedStruct = grassmoor_tracker_get_media_file_info (tracker, arg_file_name, GRASSMOOR_TRACKER_MEDIA_TYPE_UNKNOWN, NULL);
					if(MedStruct && grassmoor_media_info_get_title (MedStruct))
					{
						mildenhall_statusbar_call_set_status_bar_info (status_bar_proxy, clientAppData->appName, "NULL", clientAppData->pIconPath, g_strdup (grassmoor_media_info_get_title (MedStruct)), NULL, set_status_bar_clb, NULL);
					}
					else
						mildenhall_statusbar_call_set_status_bar_info (status_bar_proxy, clientAppData->appName, "NULL", clientAppData->pIconPath, g_strdup("Unknown"), NULL, set_status_bar_clb, NULL);
				}

				/* free media info data */
				grassmoor_media_info_free (MedStruct);
			}
		}
		else
		{
			DEBUG ("App is not registered to Agent Service");
		}

	}
}

static void play_paused_cb(TinwellPlayer *object,
		const gchar *arg_app_name,
		const gchar *arg_file_name,
		gdouble arg_track_pos)
{
	if(NULL != arg_app_name)
	{
		/* get Client data */
		FramptonAgentInternalData* clientAppData = audio_agent_get_client_struct(arg_app_name);
		if(NULL != clientAppData)
		{
			/* if current playing track is pointing same as the filename
			   only then emit the signal */
			if(g_strcmp0(clientAppData->curPlayingTrack,arg_file_name) == 0)
			{
				/* upadte the state to playing, for current playing track */
				clientAppData->playState = FRAMPTON_AGENT_PAUSED;
				/* Emit signal::play-paused to client */
				frampton_agent_emit_play_paused(pFramptonAgentObject,arg_app_name,arg_file_name,arg_track_pos);
			}
		}
		else
		{
			DEBUG ("App is not registered to Agent Service");
		}

	}
}

static void playback_error_cb(TinwellPlayer *object,
		const gchar *arg_app_name,
		const gchar *arg_file_name,
		const gchar *arg_err_msg)
{
	if(NULL != arg_app_name)
	{
		/* get Client data */
		FramptonAgentInternalData* clientAppData = audio_agent_get_client_struct(arg_app_name);
		if(NULL != clientAppData)
		{
			/* if current playing track is pointing same as the filename
			   only then emit the signal */
			if(g_strcmp0(clientAppData->curPlayingTrack,arg_file_name) == 0)
			{
				/* upadte the state to playing, for current playing track */
				clientAppData->playState = FRAMPTON_AGENT_ERROR;
				/* Emit signal::play-error to client */
				frampton_agent_emit_play_error(pFramptonAgentObject,arg_app_name,arg_file_name,arg_err_msg);
			}
			else
			{
				/* Emit signal::play-error to client */
				frampton_agent_emit_play_error(pFramptonAgentObject,arg_app_name,arg_file_name,arg_err_msg);
			}
		}
		else
		{
			DEBUG ("App not registered to Agent Service");
		}

	}
}

static void registration_info_message_cb(GObject *source_object,
				GAsyncResult *res,
				gpointer user_data)
{
	gboolean regState;
	gchar *appName = user_data;
	GError *error = NULL;

	tinwell_player_call_register_player_finish(proxyPlayer, &regState, res , &error);
	if(regState)
	{
		DEBUG ("registration to Audio service is successfull");
		if(appName)
		{
			FramptonAgentInternalData *clientData = audio_agent_get_client_struct(appName);
			if((clientData->bActive == TRUE) && (gLastUserMode == TRUE))//FRAMPTON_AGENT_PLAYING)
			{
				/* set state as playing */
				clientData->playState = FRAMPTON_AGENT_PLAYING;
				if(clientData->curPlayingTrack)
				{
					gchar **list = queryMetatracker(clientData);
					gint i = 0;

					update_album_url_list(clientData);
                    update_artist_url_list(clientData);
					while(list[i])
					{
						if(g_strcmp0(list[i],clientData->curPlayingTrack) == 0)
						{
							clientData->iPlayIndex = i;
							tinwell_player_call_start_playback(proxyPlayer,clientData->appName,clientData->curPlayingTrack,0 , NULL, play_message_info_cb, appName);
							break;
						}
						i++;
					}
				}
			}
		}
	}
	else
	{
		WARNING ("registration to Audio service is failed! error: %s", error->message);
        g_error_free (error);
	}
}

/*****************************************************************************************
 * audio_agent_free_player_album_tracks_list :
 *
 * destrying a perticular clients tracklist
 ****************************************************************************************/
static void audio_agent_free_player_album_tracks_list (FramptonAgentInternalData *clientAppData)
{
        if(NULL != clientAppData->pArrStrAlbumList)
        {
                gint i = 0;
                while(clientAppData->pArrStrAlbumList[i])
                {
                        g_free(clientAppData->pArrStrAlbumList[i]);
                        clientAppData->pArrStrAlbumList[i] = NULL;
                        i++;
                }
                g_free(clientAppData->pArrStrAlbumList);
                clientAppData->pArrStrAlbumList = NULL;
        }
}

/*****************************************************************************************
 * audio_agent_free_player_artist_tracks_list :
 *
 * destrying a perticular clients tracklist
 ****************************************************************************************/
static void audio_agent_free_player_artist_tracks_list (FramptonAgentInternalData *clientAppData)
{
        if(NULL != clientAppData->pArrStrArtistList)
        {
                gint i = 0;
                while(clientAppData->pArrStrArtistList[i])
                {
                        g_free(clientAppData->pArrStrArtistList[i]);
                        clientAppData->pArrStrArtistList[i] = NULL;
                        i++;
                }
                g_free(clientAppData->pArrStrArtistList);
                clientAppData->pArrStrArtistList = NULL;
        }
}


/**************************************************************
 * audio_agent_free_player_tracks_list :
 *
 * destrying a perticular clients tracklist
 *************************************************************/
static void audio_agent_free_tracker_list (FramptonAgentInternalData *clientAppData)
{
	/*If the history already exists purge it completely */
        if(NULL != clientAppData->songsList)
        {
                g_ptr_array_remove_range(clientAppData->songsList,0,clientAppData->songsList->len);
        }
	clientAppData->songsList = NULL;
}

/**************************************************************
 * audio_agent_free_player_tracks_list :
 *
 * destrying a perticular clients tracklist
 *************************************************************/
static void audio_agent_free_player_tracks_list (FramptonAgentInternalData *clientAppData)
{
	if(NULL != clientAppData->pArrStrSongList)
	{
		gint i = 0;
		while(clientAppData->pArrStrSongList[i])
		{
			g_free(clientAppData->pArrStrSongList[i]);
			clientAppData->pArrStrSongList[i] = NULL;
			i++;
		}
		g_free(clientAppData->pArrStrSongList);
		clientAppData->pArrStrSongList = NULL;

	}
}

/**************************************************************
 * destroy_agent_app_entry :
 *
 * method to destroy each client data from hash
 *************************************************************/
static gboolean destroy_agent_app_entry(gpointer key, gpointer value, gpointer userData)
{

        if(NULL != key && NULL != value)
        {
                FramptonAgentInternalData *agentData = (FramptonAgentInternalData*) value;

                if(NULL != agentData)
                {
                        /* Free memory held for the member variables */
                        if(NULL != agentData->appName)
                        {
                                g_free(agentData->appName);
                                 agentData->appName = NULL;
                        }
                        if(NULL != agentData->curPlayingTrack)
                        {
                                g_free(agentData->curPlayingTrack);
                                agentData->curPlayingTrack = NULL;
                        }
			if(NULL != agentData->pIconPath)
			{
				g_free(agentData->pIconPath);
				agentData->pIconPath = NULL;
			}
                        /* Free up all memory held for the list of tracks */
                        audio_agent_free_player_tracks_list(agentData);
			audio_agent_free_player_artist_tracks_list(agentData);
			audio_agent_free_player_album_tracks_list(agentData);

                        g_free(agentData);
                        agentData = NULL;
                }
        }

        return TRUE;
}

/**************************************************************
 * audio_agent_hash_deinit :
 *
 * Destroying hash data structure required for reading and writing data of all
 * clients registered to it.
 *************************************************************/
static void audio_agent_hash_deinit (void)
{
        /* Free all memory held up for the app Struct in the hash table */
        g_hash_table_foreach_remove(audioAgentAppHash, (GHRFunc) destroy_agent_app_entry, NULL);

        /* Destroy all memory allocated for the hash table */
        g_hash_table_destroy(audioAgentAppHash);

        /* Set the hash table pointer to NULL */
        audioAgentAppHash = NULL;
}

static gboolean
mount_changed_clb (PrestwoodService *object,
                   const gchar      *client_name,
                   gboolean          added,
                   const gchar      *uri,
                   gpointer          user_data)
{
	if(added)
	{	
		DEBUG (" added uri: %s",uri);
		return TRUE;
	}
	else
	{
		/* check if the device is disconnected */
		GList *keys = g_hash_table_get_keys(audioAgentAppHash);
		gint len = g_list_length(keys);
		gint i;
		for (i = 0; i<len; i++)
		{
			FramptonAgentInternalData *ClientData = (FramptonAgentInternalData *)g_hash_table_lookup(audioAgentAppHash,(gchar *)g_list_nth_data(keys,i));
			if(g_strcmp0(ClientData->volumeMountPoint,uri) == 0)
			{
				/* If device is removed, pause the track which is playing */
				if( ClientData->playState == FRAMPTON_AGENT_PLAYING)
					tinwell_player_call_pause_playback(proxyPlayer, ClientData->appName, NULL, pause_info_message_cb, NULL);
			}
		}

		DEBUG ("removed uri: %s",uri);
	}

	return TRUE;
}

static void media_proxy_clb( GObject *source_object,
                                GAsyncResult *res,
                                gpointer user_data)
{
	GError *error;
	/* finishes the proxy creation and gets the proxy ptr */
	proxyMedia = prestwood_service_proxy_new_finish(res , &error);

	if(proxyMedia == NULL)
	{
		WARNING ("Prestwood service proxy creation failed with error: %s", error->message);
        	g_error_free (error);
		return;
	}
	g_signal_connect(proxyMedia,"mount-changed",(GCallback)mount_changed_clb,user_data);
}

/**************************************************************
 * media_name_appeared:
 * @connection           : Commmand line Argument Count
 * @name                 : Name
 * @name_owner           : Array of strings
 * @user_data            :
 *
 * Callback when service found
 *************************************************************/
static void media_name_appeared (GDBusConnection *connection,
               const gchar     *name,
               const gchar     *name_owner,
               gpointer         user_data)
{
  GError *error = NULL;
  DEBUG ("service name appeared: %s", name);
  prestwood_service_proxy_new (
                                     (GDBusConnection *)connection,
                                      G_DBUS_PROXY_FLAGS_NONE,
                                      "org.apertis.Prestwood",
                                      "/org/apertis/Prestwood/Service",
                                      NULL,
                                      media_proxy_clb,
                                      &error);
  if (error)
  {
  	WARNING ("error in prestwood service proxy creation: %s", error->message);
  	g_error_free (error);
  }
  /* Asynchronously creates a proxy for the D-Bus interface */
}

/**************************************************************
 * readPdi	:
 * @voicenotes           :
 *
 * Entry point of main thread.
 *************************************************************/
static void readPdi(void)
{
	GPtrArray *gpArray = seaton_preference_get(sqliteObj, NULL, NULL,NULL );
	guint i;

	if(NULL != gpArray)
	{
		for( i = 0; i<gpArray->len ;i++)
		{
			GHashTable *hashChk;
			gchar *appName;
			FramptonAgentInternalData *clientData;

			/* get hash table */
			hashChk= g_ptr_array_index(gpArray , i);
			/* read the app name  */
			appName = g_hash_table_lookup (hashChk, "appName");

			/* create the mem to strore client data */
			clientData = audio_agent_create_client_struct(appName);

			/* store data */
			clientData->curPlayingTrack = g_strdup((gchar *)g_hash_table_lookup(hashChk,"track"));
			clientData->bShuffle = atoi(g_strdup((gchar *)g_hash_table_lookup(hashChk,"shuffle")));
			clientData->bRepeat = atoi(g_strdup((gchar *)g_hash_table_lookup(hashChk,"repeat")));
			clientData->volumeMountPoint = g_strdup((gchar *)g_hash_table_lookup(hashChk,"mountPath"));
			clientData->playState = atoi(g_strdup((gchar *)g_hash_table_lookup(hashChk,"state")));
			clientData->trackPos = atof(g_strdup((gchar *)g_hash_table_lookup(hashChk,"position")));
			clientData->volume = atoi(g_strdup((gchar *)g_hash_table_lookup(hashChk,"volume")));
			clientData->info = atoi(g_strdup((gchar *)g_hash_table_lookup(hashChk,"info")));
			clientData->queryOrder = atoi(g_strdup((gchar *)g_hash_table_lookup(hashChk,"queryOrder")));
			clientData->bActive = atoi(g_strdup((gchar *)g_hash_table_lookup(hashChk,"active")));
			clientData->pIconPath = g_strdup((gchar *)g_hash_table_lookup(hashChk,"iconPath"));
			DEBUG ("Playing track %s from position %lf requested by app: %s", clientData->curPlayingTrack, clientData->trackPos, clientData->appName);

			if((clientData->curPlayingTrack != NULL) && ((clientData->trackPos > 0.0L) && (clientData->trackPos < 1.0L)))
			{
				clientData->seek = TRUE;
			}
			
			/* Add new client details to service hash list */
			audio_agent_add_client_to_hash(appName, clientData);
			/* Every app which will register to agent will also register to audio service */
			tinwell_player_call_register_player(proxyPlayer, appName, NULL, registration_info_message_cb, appName);

			/* hash table in no more required */
			g_hash_table_destroy(hashChk);

		}
		/* free owned resources */
		g_ptr_array_free (gpArray , TRUE);

	}
	else
		DEBUG ("No data available in DB");
}

/**************************************************************
 * InitReadPdi	:
 * @voicenotes           :
 *
 * Entry point of main thread.
 *************************************************************/
static void InitReadPdi(void)
{
    gint inResult;
    SeatonFieldParam pfieldparam[12];
    sqliteObj = seaton_preference_new();
    inResult = seaton_preference_open (sqliteObj,
        FRAMPTON_BUNDLE_ID, FRAMPTON_AGENT_ID,
        SEATON_PREFERENCE_APP_INTERNAL_DB);

    if( !inResult )
    {
        DEBUG ("Data Base Created...");
    }
    else if (inResult)
    {
        WARNING ("Data Base Creation failed = %d",inResult );
    }

    pfieldparam[0].pKeyName = g_strdup("appName");
    pfieldparam[1].pKeyName = g_strdup("track");
    pfieldparam[2].pKeyName = g_strdup("shuffle");
    pfieldparam[3].pKeyName = g_strdup("repeat");
    pfieldparam[4].pKeyName = g_strdup("mountPath");
    pfieldparam[5].pKeyName = g_strdup("state");
    pfieldparam[6].pKeyName = g_strdup("position");
    pfieldparam[7].pKeyName = g_strdup("volume");
    pfieldparam[8].pKeyName = g_strdup("info");
    pfieldparam[9].pKeyName = g_strdup("queryOrder");
    pfieldparam[10].pKeyName = g_strdup("active");
    pfieldparam[11].pKeyName = g_strdup("iconPath");


    pfieldparam[0].inFieldType= SQLITE_TEXT;
    pfieldparam[1].inFieldType= SQLITE_TEXT;
    pfieldparam[2].inFieldType= SQLITE_TEXT;
    pfieldparam[3].inFieldType= SQLITE_TEXT;
    pfieldparam[4].inFieldType= SQLITE_TEXT;
    pfieldparam[5].inFieldType= SQLITE_TEXT;
    pfieldparam[6].inFieldType= SQLITE_TEXT;
    pfieldparam[7].inFieldType= SQLITE_TEXT;
    pfieldparam[8].inFieldType= SQLITE_TEXT;
    pfieldparam[9].inFieldType= SQLITE_TEXT;
    pfieldparam[10].inFieldType= SQLITE_TEXT;
    pfieldparam[11].inFieldType= SQLITE_TEXT;


    inResult =  seaton_preference_install(sqliteObj,NULL, pfieldparam, 12);

    if( !inResult )
    {
        DEBUG ("Table creation Successful ..");
    }
    else 
    {
        DEBUG ("Table creation failed = %d",inResult );
    }
	/* read from the database store the data into corresponding app*/
	readPdi();

}

/**************************************************************
 * media_name_vanished :
 *
 * callback
 *************************************************************/
static void media_name_vanished(GDBusConnection *connection,
               const gchar     *name,
               gpointer         user_data)
{
 DEBUG ("service name vanished: %s", name);

 if(NULL != proxyMedia)
 g_object_unref(proxyMedia);
}

/**************************************************************
 * statusbar_name_vanished :
 *
 * callback
 *************************************************************/
static void statusbar_name_vanished(GDBusConnection *connection,
                           const gchar     *name,
                           gpointer         user_data)
{
  DEBUG ("service name vanished: %s", name);

  if(NULL != status_bar_proxy)
    g_object_unref(status_bar_proxy);


}

/**************************************************************
 * statusbar_proxy_clb :
 *
 * callback
 *************************************************************/
static void statusbar_proxy_clb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
  GError *error=NULL;
  status_bar_proxy = mildenhall_statusbar_proxy_new_finish (res, &error);
  if (error)
  {
    WARNING ("status bar proxy creation is failed with error: %s", error->message);
    g_error_free (error);
  }

}

/**************************************************************
 * statusbar_name_appeared :
 *
 * callback
 *************************************************************/
static void statusbar_name_appeared (GDBusConnection *connection,
                            const gchar     *name,
                            const gchar     *name_owner,
                            gpointer         user_data)
{
        DEBUG ("service name appeared: %s", name);
        mildenhall_statusbar_proxy_new (connection,
                                        G_DBUS_PROXY_FLAGS_NONE,
                                        "org.apertis.Mildenhall.Statusbar",
                                        "/org/apertis/Mildenhall/Statusbar",
                                        NULL,
                                        statusbar_proxy_clb,
                                        user_data);
}

/**************************************************************
 * main	:
 * @argc           : Commmand line Argument Count
 * @argv            : Array of strings
 *
 * Entry point of main thread.
 *************************************************************/
gint main(gint argc, gchar *argv[])
{
	g_autoptr (GDBusConnection) session_bus = NULL;
	g_autoptr (GError) error = NULL;
	g_autoptr (GApplication) app = NULL;
	guint serviceWatch;
	guint mediaWatch;

	audioAgentAppHash = g_hash_table_new (g_str_hash, g_str_equal);
	tracker = grassmoor_tracker_new ();
	InitReadPdi ();
	g_unix_signal_add (SIGINT, audio_client_unregister, NULL);
	g_unix_signal_add (SIGHUP, audio_client_unregister, NULL);
	g_unix_signal_add (SIGTERM, audio_client_unregister, NULL);

	/* get last user mode for player application */
	if(NULL != argv)
	{
		gint i = 0;
		while (argv[i])
		{
			if(g_strcmp0(argv[i],"play-mode") == 0)
			{
				if(g_strcmp0(argv[i + 1],"play") == 0)
				{
					gLastUserMode = TRUE;
					break;
				}
			}
			i++;
		}

	}

	app = G_APPLICATION (frampton_service_new ());

	/* We can't implement our own D-Bus APIs until we've contacted Tinwell,
	 * so there is no down side to doing this synchronously. */
	session_bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);

	if (session_bus == NULL)
	{
		CRITICAL ("Unable to connect to session bus: %s", error->message);
		return EXIT_FAILURE;
	}

	proxyPlayer = tinwell_player_proxy_new_sync (session_bus,
		G_DBUS_PROXY_FLAGS_NONE, "org.apertis.Tinwell",
		"/org/apertis/Tinwell/Player", NULL, &error);

	if (proxyPlayer == NULL)
	{
		CRITICAL ("Unable to contact Tinwell: %s", error->message);
		return EXIT_FAILURE;
	}

	g_signal_connect (proxyPlayer, "playback-started", G_CALLBACK (play_started_cb), NULL);
	g_signal_connect (proxyPlayer, "playback-completed", G_CALLBACK (play_completed_cb), NULL);
	g_signal_connect (proxyPlayer, "playback-paused", G_CALLBACK (play_paused_cb), NULL);
	g_signal_connect (proxyPlayer, "playback-resumed", G_CALLBACK (play_resumed_cb), NULL);
	g_signal_connect (proxyPlayer, "playback-position", G_CALLBACK (play_position_cb), NULL);
	g_signal_connect (proxyPlayer, "playback-error", G_CALLBACK (playback_error_cb), NULL);

	pFramptonAgentObject = frampton_agent_skeleton_new ();
	g_return_val_if_fail (pFramptonAgentObject != NULL, EXIT_FAILURE);
	connect_signal_handler_for_methods (pFramptonAgentObject);

	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (pFramptonAgentObject),
				session_bus, FRAMPTON_AGENT_OBJECT_PATH, &error))
	{
		CRITICAL ("Unable to export FramptonAgent API: %s",
			error->message);
		return EXIT_FAILURE;
	}

	/* Starts watching name on the bus specified by bus_type */
	/* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
	mediaWatch = g_bus_watch_name (G_BUS_TYPE_SESSION,
			"org.apertis.Prestwood",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			media_name_appeared,
			media_name_vanished,
			NULL,
			NULL);

	/* Starts watching name on the bus specified by bus_type */
	/* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
	g_bus_watch_name (G_BUS_TYPE_SESSION,
                        "org.apertis.Mildenhall.Statusbar",
                        G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                        statusbar_name_appeared,
                        statusbar_name_vanished,
                        NULL,
                        NULL);
	(void)mediaWatch;
	(void)serviceWatch;

	g_application_run (app, argc, argv);

	g_bus_unown_name(uinOwnerId);
	g_object_unref (tracker);
	exit (EXIT_SUCCESS);
}

