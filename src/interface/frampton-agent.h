/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FRAMPTON_AGENT_H__
#define __FRAMPTON_AGENT_H__

#include "frampton-agent-iface.h"

#define FRAMPTON_BUNDLE_ID "org.apertis.Frampton"
#define FRAMPTON_APP_ID FRAMPTON_BUNDLE_ID
#define FRAMPTON_AGENT_ID FRAMPTON_BUNDLE_ID ".Agent"
#define FRAMPTON_AGENT_BUS_NAME FRAMPTON_AGENT_ID
#define FRAMPTON_AGENT_OBJECT_PATH "/org/apertis/Frampton/Agent"

typedef enum {
	FRAMPTON_AGENT_NONE,
	FRAMPTON_AGENT_PLAYING,
	FRAMPTON_AGENT_PAUSED,
	FRAMPTON_AGENT_ERROR,
}enPlayState;

/* wrapper on libgrassmoor-tracker volume type */
typedef enum
{
	FRAMPTON_AGENT_VOLUME_LOCAL = 1,
	FRAMPTON_AGENT_VOLUME_REMOVABLE,
} enFramptonAgentVolumeType;

/* Enum to identify the sorting order url list*/
enum
{
        FRAMPTON_AGENT_SORT_ASCENDING ,
        FRAMPTON_AGENT_SORT_DESCENDING,
}enFramptonAgentSortType;

/**
 * enFramptonAgentMediaType:
 * @FRAMPTON_AGENT_MEDIA_TYPE_FRAMPTON: Media File Type for Audio Files
 * This enum value is Required for asking information or list of particular media type
 **/
typedef enum
{
	FRAMPTON_AGENT_MEDIA_TYPE_FRAMPTON,
} enFramptonAgentMediaType;

typedef enum
{
        FRAMPTON_AGENT_MEDIA_INFO_URL,
        FRAMPTON_AGENT_MEDIA_INFO_TITLE = 4,
        FRAMPTON_AGENT_MEDIA_INFO_ARTIST,
        FRAMPTON_AGENT_MEDIA_INFO_ALBUM,
} enInfoType;

#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)


#endif /* __FRAMPTON_AGENT_H__ */
