/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "thumbnailitem.h"
#include "liblightwood-fixedroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"
#include <mildenhall/mildenhall.h>

/* private members */
typedef struct
{
  ClutterActor *pBox;
  ClutterActor *pText;
  ClutterActor *mediaOverlay;
  gboolean bShow;
} FramptonThumbnailItemPrivate;

struct _FramptonThumbnailItem
{
  ClutterActor parent;
};

G_DEFINE_TYPE_WITH_PRIVATE (FramptonThumbnailItem, frampton_thumbnail_item, CLUTTER_TYPE_ACTOR);

#define THUMBNAIL_ITEM_PRINT( a ...)                //g_print(a) 

enum
{
        PROP_FIRST,
        PROP_IMAGE_CONTENT,
        PROP_OVERLAY_TEXT,
        PROP_MEDIA_STATE,
	PROP_MEDIA_SHOW,
	PROP_WIDTH,
	PROP_HEIGHT,	
        PROP_LAST
};

/********************************************************
 * Function : thumbnail_item_set_property
 * Description: set a property value 
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void thumbnail_item_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec)
{
        FramptonThumbnailItem *thumbnailItem = FRAMPTON_THUMBNAIL_ITEM (pObject);
	FramptonThumbnailItemPrivate *priv = frampton_thumbnail_item_get_instance_private (thumbnailItem);
	gboolean bFocus = FALSE;
	gboolean bShow = FALSE;


	switch(uinPropertyID)
	{	
		case PROP_IMAGE_CONTENT:
			clutter_actor_set_content (priv->pBox, CLUTTER_CONTENT (g_value_get_object (pValue)) );
			break;
		case PROP_OVERLAY_TEXT:
			if(NULL != g_value_get_string(pValue))
			{
				g_object_set(priv->mediaOverlay, "overlay-text", g_value_get_string(pValue), NULL);
			}
			break;
		case PROP_MEDIA_SHOW:
			bShow = g_value_get_boolean(pValue);
			if(bShow)
			{
				clutter_actor_show(priv->mediaOverlay);
			}
			else
				clutter_actor_hide(priv->mediaOverlay);
			break;
		case PROP_MEDIA_STATE:
			bFocus = g_value_get_boolean(pValue);
			g_object_set(priv->mediaOverlay, "play-state", bFocus, NULL);
			break;
		 case PROP_WIDTH:
			g_object_set(priv->mediaOverlay, "scale", g_value_get_float(pValue), NULL); 
			clutter_actor_set_width(priv->pBox,  g_value_get_float(pValue));
                        break;
                case PROP_HEIGHT:
			g_object_set(priv->mediaOverlay, "scale", g_value_get_float(pValue), NULL);
			clutter_actor_set_height(priv->pBox,  g_value_get_float(pValue));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : thumbnail_item_get_property
 * Description: set a property value 
 :* Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void thumbnail_item_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec)
{
        FramptonThumbnailItem *thumbnailItem = FRAMPTON_THUMBNAIL_ITEM (pObject);
        FramptonThumbnailItemPrivate *priv = frampton_thumbnail_item_get_instance_private (thumbnailItem);
	gboolean bFocus = FALSE;

	switch(uinPropertyID)
	{
		case PROP_IMAGE_CONTENT:
			g_value_set_object (pValue, clutter_actor_get_content (priv->pBox));
			break;
		case PROP_OVERLAY_TEXT:
			{
				gchar *overlay_text = NULL;
				g_object_get (priv->mediaOverlay, "overlay-text", &overlay_text, NULL);
				g_value_take_string (pValue, overlay_text);	
			}
			break;
		 case PROP_WIDTH:
			g_value_set_float(pValue, clutter_actor_get_width(priv->pBox));
                        break;
                case PROP_HEIGHT:
			g_value_set_float(pValue, clutter_actor_get_height(priv->pBox));
			break;
		case PROP_MEDIA_SHOW:
			if (clutter_actor_is_visible (priv->mediaOverlay))
				g_value_set_boolean(pValue, TRUE);	
			else
				g_value_set_boolean(pValue, FALSE);
			break;
		case PROP_MEDIA_STATE:
                        g_object_get(priv->mediaOverlay, "play-state", &bFocus, NULL);
			g_value_set_boolean(pValue, bFocus);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : thumbnail_item_finalize
 * Description: Finalize the thumbnail object 
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void thumbnail_item_finalize (GObject *pObject)
{
	FramptonThumbnailItem *thumbnailItem = FRAMPTON_THUMBNAIL_ITEM (pObject);
        FramptonThumbnailItemPrivate *priv = frampton_thumbnail_item_get_instance_private (thumbnailItem);

	clutter_actor_destroy (priv->pBox);
	clutter_actor_destroy (priv->mediaOverlay);

        G_OBJECT_CLASS (frampton_thumbnail_item_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : thumbnail_item_dispose
 * Description: Dispose the thumbnail object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void thumbnail_item_dispose (GObject *pObject)
{
  G_OBJECT_CLASS (frampton_thumbnail_item_parent_class)->dispose (pObject);
}

static void
frampton_thumbnail_item_class_init (FramptonThumbnailItemClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);

	GParamSpec *pspec = NULL;

        pObjectClass->get_property = thumbnail_item_get_property;
        pObjectClass->set_property = thumbnail_item_set_property;
        pObjectClass->dispose = thumbnail_item_dispose;
        pObjectClass->finalize = thumbnail_item_finalize;

	/**
         * FramptonThumbnailItem:image-content:
         *
         * ClutterContent object to create thumbnail actor
         * 
         * Default:
         */
	pspec = g_param_spec_object ("image-content",
                              "Image-Content",
                              "texture content for the thumbnail icon",
                              G_TYPE_OBJECT,
                              G_PARAM_READWRITE);

	g_object_class_install_property (pObjectClass, PROP_IMAGE_CONTENT, pspec);

	/**
         * FramptonThumbnailItem:width:
         *
         * Width for the item
         * 
         * Default: 0.0
         */
        pspec = g_param_spec_float ("width",
                        "Width",
                        "Width of the item",
			-G_MAXFLOAT,
                        G_MAXFLOAT,
                        0.0,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_WIDTH, pspec);

	/**
         * FramptonThumbnailItem:height:
         *
         * Height for the item
         * 
         * Default: 0.0
         */
        pspec = g_param_spec_float ("height",
                        "Height",
                        "Height of the item",
			-G_MAXFLOAT,
                              G_MAXFLOAT,
                              0.0,
                        G_PARAM_READWRITE);
        g_object_class_install_property ( pObjectClass, PROP_HEIGHT, pspec);

	/**
         * FramptonThumbnailItem:media-state:
         *
         *  Play/Pause state for media overla
         * 
         * Default: FALSE
         */
	pspec = g_param_spec_boolean ("media-state",
                        "Media-Play",
                        "Play or Pause media overlay",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MEDIA_STATE, pspec);
	
	/**
         * FramptonThumbnailItem:show:
         *
         *  Show or Hide media overlay
         * 
         * Default: FALSE
         */
	pspec = g_param_spec_boolean ("show",
                        "Media-Overlay-Show",
                        "Show or Hide media overlay",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MEDIA_SHOW, pspec);

	/**
         * FramptonThumbnailItem:overlay-text:
         *
         *  Tttle text for  media overlay
         * 
         * Default: NULL
         */
	pspec = g_param_spec_string ("overlay-text",
                        "Media-Overlay-text",
                        "Text for  media overlay",
                        NULL,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_OVERLAY_TEXT, pspec);
}



static void
frampton_thumbnail_item_init (FramptonThumbnailItem *thumbnailItem)
{
	FramptonThumbnailItemPrivate *priv = frampton_thumbnail_item_get_instance_private (thumbnailItem);

	priv->bShow = FALSE;

	priv->pBox = clutter_actor_new();
        clutter_actor_set_size (priv->pBox, 164, 164);
        clutter_actor_add_child (CLUTTER_ACTOR (thumbnailItem), priv->pBox);

	priv->mediaOverlay = g_object_new(MILDENHALL_TYPE_MEDIA_OVERLAY, NULL);
        clutter_actor_add_child(CLUTTER_ACTOR(thumbnailItem), priv->mediaOverlay);
	clutter_actor_hide(priv->mediaOverlay);

	clutter_actor_set_reactive(CLUTTER_ACTOR (thumbnailItem), TRUE);
}

FramptonThumbnailItem *
frampton_thumbnail_item_new (void)
{
  return g_object_new (FRAMPTON_TYPE_THUMBNAIL_ITEM, NULL);
}


