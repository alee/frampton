/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "frampton-app.h"

#include "frampton.h"
#include "frampton-mode.h"

/*
 * FramptonApp:
 *
 * Object encapsulating the life-cycle of the Frampton GUI process.
 *
 * FIXME: Most of the logic from frampton-main.c should eventually be in here,
 * and most of the global variables should be members of this object.
 * However, for the moment it remains in frampton-main.c, which listens for
 * this object's ::activate, ::open, ::command-line signals instead of
 * implementing the corresponding virtual methods here.
 */

struct _FramptonApp
{
  GApplication parent;
  FramptonMode *modes[FRAMPTON_AUDIO_PLAYER_MODE_NONE];
};

G_DEFINE_TYPE (FramptonApp, frampton_app, G_TYPE_APPLICATION)

enum
{
  SIGNAL_MODE_ACTIVATED,
  LAST_SIGNAL
};

static guint frampton_app_signals[LAST_SIGNAL] = { 0 };

static void
activate_cb (FramptonMode *app,
             gpointer user_data)
{
  FramptonApp *self = FRAMPTON_APP (user_data);
  guint i;

  for (i = 0; i < G_N_ELEMENTS (self->modes); i++)
    {
      if (app == self->modes[i])
        {
          const gchar *id;

          id = g_application_get_application_id (G_APPLICATION (app));
          g_signal_emit (self, frampton_app_signals[SIGNAL_MODE_ACTIVATED],
                         0, id, i);
        }
    }
}

static void
frampton_app_init (FramptonApp *self)
{
}

static void
frampton_app_constructed (GObject *object)
{
  FramptonApp *self = FRAMPTON_APP (object);
  GObjectClass *parent = G_OBJECT_CLASS (frampton_app_parent_class);
  guint i;

  /* This sets the "main" FramptonApp as the default GApplication.
   * We want this one to be the default, not one of the modes. */
  parent->constructed (object);

  for (i = 0; i < G_N_ELEMENTS (self->modes); i++)
    self->modes[i] = frampton_mode_new (i);

  g_signal_connect_object (self->modes[FRAMPTON_AUDIO_PLAYER_MODE_ARTIST],
                           "activate", G_CALLBACK (activate_cb),
                           self, 0);

  g_signal_connect_object (self->modes[FRAMPTON_AUDIO_PLAYER_MODE_ALBUM],
                           "activate", G_CALLBACK (activate_cb),
                           self, 0);

  g_signal_connect_object (self->modes[FRAMPTON_AUDIO_PLAYER_MODE_SONGS],
                           "activate", G_CALLBACK (activate_cb),
                           self, 0);
}

static void
frampton_app_startup (GApplication *app)
{
  FramptonApp *self = FRAMPTON_APP (app);
  GApplicationClass *parent = G_APPLICATION_CLASS (frampton_app_parent_class);
  guint i;

  if (parent->startup != NULL)
    parent->startup (app);

  for (i = 0; i < G_N_ELEMENTS (self->modes); i++)
    {
      g_autoptr (GError) error = NULL;

      if (!g_application_register (G_APPLICATION (self->modes[i]), NULL,
                                   &error))
        WARNING ("Unable to register secondary GApplication: %s",
                 error->message);
    }
}

static void
frampton_app_dispose (GObject *object)
{
  FramptonApp *self = FRAMPTON_APP (object);
  GObjectClass *parent = G_OBJECT_CLASS (frampton_app_parent_class);
  guint i;

  for (i = 0; i < G_N_ELEMENTS (self->modes); i++)
    g_clear_object (&self->modes[i]);

  parent->dispose (object);
}

static void
frampton_app_class_init (FramptonAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->constructed = frampton_app_constructed;
  object_class->dispose = frampton_app_dispose;

  app_class->startup = frampton_app_startup;

  /*
   * mode-activated:
   * @entry_point_id: The non-main entry point ID representing the mode
   * @mode: The FramptonAudioPlayerMode for the mode
   *
   * Emitted when we switch to another mode. This is analogous to
   * the ::activated signal (the other modes do not implement ::open or
   * ::command-line).
   */
  frampton_app_signals[SIGNAL_MODE_ACTIVATED] =
      g_signal_new ("mode-activated", G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                    G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_INT);
}

FramptonApp *
frampton_app_new (void)
{
  GApplicationFlags flags = (G_APPLICATION_HANDLES_OPEN |
                             G_APPLICATION_HANDLES_COMMAND_LINE);

  return FRAMPTON_APP (g_object_new (FRAMPTON_TYPE_APP,
                                     "application-id", FRAMPTON_APP_ID,
                                     "flags", flags,
                                     NULL));
}
