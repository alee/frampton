/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/******************************************************************************
 *  @Filename :   frampton_audio_player_item.c
 *  @Project: --
 *-----------------------------------------------------------------------------
 *  @Created on :  Feb 01, 2013
 *------------------------------------------------------------------------------
 *  @Description:  This widget can be used to create roller item type for roller
 *                 having two text fields at one time with different positions.
 *
 *
 * Description of FIXES:
 * -----------------------------------------------------------------------------------
 *      Description                             Date                    Name
 *      ----------                              ----                    ----
 *
 *******************************************************************************/

#include "frampton_audio_player_item.h"

#include <thornbury/thornbury.h>

#include <liblightwood-roller.h>
#include "liblightwood-fixedroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"
#include "liblightwood-glowshader.h"

#include <mildenhall/mildenhall.h>

/* private members */
typedef struct _FramptonAudioPlayerItemPrivate
{
	ClutterActor *pThumbNail;
	ClutterActor *pMidText;
	ClutterActor *pRightText;
	ClutterActor *pVerticalLine;
	ClutterActor *pBottomLine;
	ClutterActor *pTopLine;
	ClutterActor *pFooterBg;
	ClutterActor *pArrow;
	ClutterActor *coglTexture;
	ClutterActor *pBox;

	gchar *pThumbNailPath;

	gboolean bFooter;
	gboolean bArrowUp;
	gboolean row_called;

	/* style property */
	gchar *pFooterImagePath;
	gchar *pArrowUpMarkup;
	gchar *pArrowDownMarkup;
	gchar *pFont;
	gfloat flIconHeight;
	gfloat flIconWidth;
	gfloat flMidX;
	gfloat flMidWidth;
	gfloat flRightX;
	gfloat flTextY;
	gfloat flRightWidth;
	gfloat flVerticalLineX;
	gfloat flLineX;
	gfloat flBottomLineY;
	gfloat flArrowX;
	gfloat flHorizontalLineWidth;
	gfloat flFooterHeight;

	ClutterColor textColor;
	ClutterColor lineColor;

	ClutterEffect *glow_effect_1;
	ClutterEffect *glow_effect_2;
} FramptonAudioPlayerItemPrivate;

struct _FramptonAudioPlayerItem
{
  ClutterActor parent;
};

G_DEFINE_TYPE_WITH_PRIVATE (FramptonAudioPlayerItem, frampton_audio_player_item, CLUTTER_TYPE_ACTOR)

#define FRAMPTON_AUDIO_PLAYER_ITEM_PRINT( a ...)                //g_print(a)

/* style properties */
#define FOOTER_IMAGE		"footer-image"
#define ARROW_UP		"arrow-up-markup"
#define ARROW_DOWN		"arrow-down-markup"
#define ICON_PATH		"icon-path"
#define ICON_WIDTH		"icon-width"
#define ICON_HEIGHT		"icon-height"
#define MID_X                "mid-x"
#define TEXT_Y                  "text-y"
#define RIGHT_X       		"right-x"
#define MID_WIDTH            "mid-width"
#define RIGHT_WIDTH     	"right-width"
#define VERTICAL_LINE_X         "vertical-line-x"
#define LINE_X        		"line-x"
#define BOTTOM_LINE_Y     	"bottom-line-y"
#define HORIZONTAL_LINE_WIDTH 	"horizontal-line-width"
#define TEXT_FONT         	"font"
#define LINE_COLOR         	"line-color"
#define TEXT_COLOR         	"text-color"
#define FOOTER_HEIGHT         	"footer-height"
#define ARROW_X      		"arrow-x"

/* property */
enum
{
        PROP_FIRST,
        PROP_ICON,
	PROP_CONTENT,
        PROP_MID_TEXT,
        PROP_RIGHT_TEXT,
	PROP_ROW,
	PROP_FOCUSED,
	PROP_FOOTER,
	PROP_ARROW_UP,
	PROP_SHOW_ARROW,
        PROP_LAST
};

static void create_footer(FramptonAudioPlayerItem *pAudioPlayerItem, gfloat flRollerWidth);
/*******************************************************************************
 * Internal Functions
 ******************************************************************************/

/*******************************************************
 * Function : on_notify_language
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void on_notify_language ( GObject    *object,
                                 GParamSpec *pspec,
                                 gpointer    user_data)
{
	const gchar *midText = NULL;
	const gchar *rightText = NULL;

        FramptonAudioPlayerItem *pAudioPlayerItem = FRAMPTON_AUDIO_PLAYER_ITEM(user_data);
        FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);

	g_return_if_fail (FRAMPTON_IS_AUDIO_PLAYER_ITEM (user_data));

        midText = clutter_text_get_text (CLUTTER_TEXT (priv->pMidText));
        rightText = clutter_text_get_text (CLUTTER_TEXT (priv->pRightText));

        FRAMPTON_AUDIO_PLAYER_ITEM_PRINT("get text =%s####\n get text = %s####\n",  gettext(midText), gettext(rightText));

        if(NULL != midText && priv->bFooter)
	{
		const gchar *text = gettext (midText);
                clutter_text_set_text(CLUTTER_TEXT(priv->pMidText), text);
	}
        if(NULL != rightText && priv->bFooter)
	{
        	const gchar *text = gettext (rightText);
		clutter_text_set_text(CLUTTER_TEXT(priv->pRightText), text );
	}

	FRAMPTON_AUDIO_PLAYER_ITEM_PRINT("%f\n", clutter_actor_get_height(CLUTTER_ACTOR(pAudioPlayerItem)) );
}

/****************************************************
 * Function : create_text
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static  void create_text(ClutterActor *pLabel, const gchar *pText, gfloat flX, gfloat flY, gfloat flWidth, FramptonAudioPlayerItem *pAudioPlayerItem)
{
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);

	g_object_set(pLabel, "x", flX, "y", flY, "width", flWidth, "font-name", priv->pFont, "color", &priv->textColor, "reactive", FALSE, "ellipsize", PANGO_ELLIPSIZE_END, "text", gettext((gchar *)pText), NULL);
}

/****************************************************
 * Function : draw_line
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static ClutterActor *draw_line(gfloat flX, gfloat flY, gfloat flWidth, gfloat flHeight, ClutterColor color)
{
        ClutterActor *pLine = clutter_actor_new();
	g_object_set(pLine, "background-color", &color, NULL);
        clutter_actor_set_position (pLine, flX, flY);
        clutter_actor_set_size (pLine, flWidth, flHeight);

        return pLine;
}

/****************************************************
 * Function : frampton_audio_player_item_set_icon
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void frampton_audio_player_item_set_icon(FramptonAudioPlayerItem *pAudioPlayerItem, const gchar *pIconPath)
{
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);
	if(! priv->bFooter)
		thornbury_ui_texture_set_from_file (priv->pThumbNail, pIconPath, priv->flIconWidth, priv->flIconHeight, TRUE, FALSE);
	else
	{
		thornbury_ui_texture_set_from_file (priv->pThumbNail, pIconPath, 32, 32, TRUE, FALSE);
		clutter_actor_set_position(priv->pThumbNail, (priv->flIconWidth - clutter_actor_get_width(priv->pThumbNail)) / 2,
				(priv->flIconHeight - clutter_actor_get_height(priv->pThumbNail)) / 2) ;
	}
}

/****************************************************
 * Function : frampton_audio_player_item_set_mid_text
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void frampton_audio_player_item_set_mid_text(FramptonAudioPlayerItem *pAudioPlayerItem, const gchar *pText)
{
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);

	create_text(priv->pMidText, pText, priv->flMidX, priv->flTextY, priv->flMidWidth, pAudioPlayerItem);
}

/****************************************************
 * Function : frampton_audio_player_item_set_right_text
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void frampton_audio_player_item_set_right_text(FramptonAudioPlayerItem *pAudioPlayerItem, const gchar *pText)
{
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);

        create_text(priv->pRightText, pText, priv->flRightX, priv->flTextY, priv->flRightWidth, pAudioPlayerItem);
}

static void frampton_audio_player_item_set_row(FramptonAudioPlayerItem *pAudioPlayerItem, guint inRow)
{
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);
	ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (pAudioPlayerItem));

	if(parent != NULL)
	{
		ThornburyModel * model = lightwood_roller_get_model(LIGHTWOOD_ROLLER(parent));
		gfloat flRollerWidth;
		clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent), -1, NULL, &flRollerWidth);
		FRAMPTON_AUDIO_PLAYER_ITEM_PRINT("$$$$$$$$$%f\n", flRollerWidth);
		clutter_actor_set_size (priv->pTopLine, (guint) flRollerWidth, 1);
		clutter_actor_show (priv->pTopLine);

		if (parent != NULL &&
				model != NULL &&
				inRow == thornbury_model_get_n_rows (model) - 1)
		{
			/* add bottom line for first item */
			clutter_actor_set_size (priv->pBottomLine, (guint) flRollerWidth, 1);
			clutter_actor_show (priv->pBottomLine);
		}
		else
			clutter_actor_hide (priv->pBottomLine);
	}
}

static void frampton_audio_player_item_set_focus(FramptonAudioPlayerItem *pAudioPlayerItem, gboolean bFocus)
{
        FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);
	if (bFocus)
	{
		if (clutter_actor_get_effect (priv->pMidText, "glow") == NULL)
			clutter_actor_add_effect_with_name (priv->pMidText, "glow", priv->glow_effect_1);
		if (clutter_actor_get_effect (priv->pRightText, "glow") == NULL)
			clutter_actor_add_effect_with_name (priv->pRightText, "glow", priv->glow_effect_2);
	}
	else
	{
		if (clutter_actor_get_effect (priv->pMidText, "glow") != NULL)
			clutter_actor_remove_effect_by_name (priv->pMidText, "glow");
		if (clutter_actor_get_effect (priv->pRightText, "glow") != NULL)
			clutter_actor_remove_effect_by_name (priv->pRightText, "glow");
	}
}

static void frampton_audio_player_item_set_footer(FramptonAudioPlayerItem *pAudioPlayerItem, gboolean bFooter)
{
        FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);
	priv->bFooter = bFooter;
	if(priv->bFooter)
	{
		gfloat flRollerWidth;
		ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (pAudioPlayerItem));
		if(parent != NULL)
		{
			if(FALSE == priv->row_called)
			{
				if (MILDENHALL_IS_ROLLER_CONTAINER (clutter_actor_get_parent (parent)))
				{
					ClutterActor *actor = clutter_actor_get_parent(parent);
					g_signal_connect(MILDENHALL_ROLLER_CONTAINER (actor),
							"notify::language",
							G_CALLBACK (on_notify_language),
							pAudioPlayerItem);
				}
			}
			priv->row_called=TRUE;
			clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent), -1, &flRollerWidth, NULL);
			create_footer(pAudioPlayerItem, flRollerWidth);
		}
	}
	else
	{
		if(priv->pFooterBg)
			clutter_actor_hide(priv->pFooterBg);
	}
}

/****************************************************
 * Function : add_footer_sort_arrow
 * Description: add arrow to footer
 * Parameters: Pointer to item and flag to indicate
 *             if it is an up or a down arrow
 * Return value: void
 ********************************************************/
static void add_footer_sort_arrow(FramptonAudioPlayerItem *pAudioPlayerItem, gboolean upArrow)
{
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);

	if(NULL == priv->pArrow)
	{
       		priv->pArrow = clutter_text_new ();
        	clutter_text_set_editable (CLUTTER_TEXT (priv->pArrow), FALSE);
        	clutter_text_set_font_name (CLUTTER_TEXT (priv->pArrow),priv->pFont );
        	clutter_text_set_color (CLUTTER_TEXT (priv->pArrow), &priv->textColor);
        	clutter_text_set_use_markup (CLUTTER_TEXT (priv->pArrow), TRUE);
	        clutter_actor_add_child(CLUTTER_ACTOR(pAudioPlayerItem), priv->pArrow);
        	clutter_actor_set_position(priv->pArrow,priv->flArrowX, /* 190,*/ priv->flTextY);

	}
	priv->bArrowUp = upArrow;

        if(upArrow)
        {
                clutter_text_set_markup (CLUTTER_TEXT (priv->pArrow), priv->pArrowUpMarkup);//"&#x25b5;");
        }
        else
        {
                clutter_text_set_markup (CLUTTER_TEXT (priv->pArrow), priv->pArrowDownMarkup);//"&#x25bf;");
        }
        clutter_actor_show(priv->pArrow);
}

/****************************************************
 * Function : create_footer
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void create_footer(FramptonAudioPlayerItem *pAudioPlayerItem, gfloat flRollerWidth)
{
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);
	ClutterActor *pLineH1;
	ClutterActor *pLineH2;
	ClutterActor *pLineV1;

	if (NULL == priv->pFooterBg && NULL != priv->pFooterImagePath)
	{
		FRAMPTON_AUDIO_PLAYER_ITEM_PRINT ("#######%s %f\n", priv->pFooterImagePath, flRollerWidth);
		priv->pFooterBg = thornbury_ui_texture_create_new (priv->pFooterImagePath, flRollerWidth, priv->flFooterHeight, FALSE, TRUE);
		clutter_actor_insert_child_at_index (CLUTTER_ACTOR (pAudioPlayerItem), priv->pFooterBg, 0);
		/* hide top and bottom line */
		if (priv->pTopLine)
			clutter_actor_hide (priv->pTopLine);
		if (priv->pBottomLine)
			clutter_actor_hide (priv->pBottomLine);

		/* draw two horizontal line */
		pLineH1 = draw_line (0, 0, flRollerWidth, 1, priv->lineColor);
		pLineH2 = draw_line (0, 7, flRollerWidth, 1, priv->lineColor);
		/* draw vertical line */
		pLineV1 = draw_line (priv->flIconWidth, 8, 1, priv->flIconHeight - 7, priv->lineColor);

		clutter_actor_add_child(CLUTTER_ACTOR(pAudioPlayerItem), pLineH1);
		clutter_actor_add_child(CLUTTER_ACTOR(pAudioPlayerItem), pLineH2);
		clutter_actor_add_child(CLUTTER_ACTOR(pAudioPlayerItem), pLineV1);
		/* reduce vertical line length */
		clutter_actor_set_y (priv->pVerticalLine, 8);
		clutter_actor_set_height (priv->pVerticalLine, clutter_actor_get_height (priv->pVerticalLine) - 7 );
		clutter_actor_show (priv->pFooterBg);
		/* add sort arrow */
		add_footer_sort_arrow(pAudioPlayerItem, TRUE);
	}
}

/********************************************************
 * Function : v_audio_player_item_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_audio_player_item_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
        FramptonAudioPlayerItem *pAudioPlayerItem = FRAMPTON_AUDIO_PLAYER_ITEM(pUserData);
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);

	if (g_strcmp0(pStyleKey, FOOTER_IMAGE) == 0)
        {
                 priv->pFooterImagePath = g_strdup_printf(PKGDATADIR"/%s", (gchar*)g_value_get_string(pValue));
        }
	if (g_strcmp0(pStyleKey, ICON_PATH) == 0)
        {
                priv->pThumbNailPath = g_strdup_printf(PKGDATADIR"/%s", (gchar*)g_value_get_string(pValue));
		FRAMPTON_AUDIO_PLAYER_ITEM_PRINT("%s\n", priv->pThumbNailPath);
        }

	else if(g_strcmp0(pStyleKey, ARROW_UP) == 0)
	{
		priv->pArrowUpMarkup = g_strdup(g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, ARROW_DOWN) == 0)
	{
		priv->pArrowDownMarkup = g_strdup( g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, ICON_WIDTH) == 0)
	{
                 priv->flIconWidth = g_value_get_double(pValue);
	}
	else if(g_strcmp0(pStyleKey, ICON_HEIGHT) == 0)
	{
                 priv->flIconHeight = g_value_get_double(pValue);
	}
	else if(g_strcmp0(pStyleKey, VERTICAL_LINE_X) == 0)
	{
                 priv->flVerticalLineX = g_value_get_double(pValue);
	}
	else if(g_strcmp0(pStyleKey, LINE_X) == 0)
	{
                 priv->flLineX = g_value_get_double(pValue);
	}
	else if(g_strcmp0(pStyleKey, BOTTOM_LINE_Y) == 0)
	{
                 priv->flBottomLineY = g_value_get_double(pValue);
	}
	else if(g_strcmp0(pStyleKey, HORIZONTAL_LINE_WIDTH) == 0)
	{
                 priv->flHorizontalLineWidth = g_value_get_double(pValue);
	}
	if(g_strcmp0(pStyleKey, LINE_COLOR) == 0)
        {
                 clutter_color_from_string(&priv->lineColor, g_value_get_string(pValue));
        }
	if(g_strcmp0(pStyleKey, TEXT_COLOR) == 0)
        {
                 clutter_color_from_string(&priv->textColor, g_value_get_string(pValue));
        }

	else if(g_strcmp0(pStyleKey, TEXT_FONT) == 0)
	{
                priv->pFont = g_strdup(g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, MID_X ) == 0)
        {
                 priv->flMidX = g_value_get_double(pValue);
        }
        else if(g_strcmp0(pStyleKey, MID_WIDTH) == 0)
        {
                 priv->flMidWidth = g_value_get_double(pValue);
        }
	else if(g_strcmp0(pStyleKey, RIGHT_X ) == 0)
        {
                 priv->flRightX = g_value_get_double(pValue);
        }
        else if(g_strcmp0(pStyleKey, TEXT_Y) == 0)
        {
                 priv->flTextY = g_value_get_double(pValue);
        }
        else if(g_strcmp0(pStyleKey, RIGHT_WIDTH) == 0)
        {
                 priv->flRightWidth = g_value_get_double(pValue);
        }
        else if(g_strcmp0(pStyleKey, ARROW_X) == 0)
        {
                 priv->flArrowX = g_value_get_double(pValue);
        }
        else if(g_strcmp0(pStyleKey, FOOTER_HEIGHT) == 0)
        {
                 priv->flFooterHeight = g_value_get_double(pValue);
        }

	g_free (pStyleKey);
}

/********************************************************
 * Function : frampton_audio_player_item_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void frampton_audio_player_item_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec)
{
	FramptonAudioPlayerItem *pAudioPlayerItem = FRAMPTON_AUDIO_PLAYER_ITEM(pObject);
        FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);
	gboolean bArrowUp = FALSE;
	gboolean bShowArrow = FALSE;

	switch (uinPropertyID)
	{
		case PROP_ICON:
			frampton_audio_player_item_set_icon(pAudioPlayerItem, g_value_get_string(pValue));
			break;
		case PROP_CONTENT:
			clutter_actor_set_content (priv->pBox, CLUTTER_CONTENT(g_value_get_object(pValue)));
			break;
		case PROP_MID_TEXT:
			frampton_audio_player_item_set_mid_text(pAudioPlayerItem, g_value_get_string(pValue));
			break;
		case PROP_RIGHT_TEXT:
			frampton_audio_player_item_set_right_text(pAudioPlayerItem, g_value_get_string(pValue));
			break;
		case PROP_ROW:
			frampton_audio_player_item_set_row(pAudioPlayerItem, g_value_get_uint (pValue) );
			break;
        case PROP_FOCUSED:
			frampton_audio_player_item_set_focus(pAudioPlayerItem, g_value_get_boolean (pValue) );
			break;
		case PROP_FOOTER:
			frampton_audio_player_item_set_footer(pAudioPlayerItem, g_value_get_boolean(pValue));
			break;
		case PROP_ARROW_UP:
			bArrowUp = g_value_get_boolean(pValue);
			add_footer_sort_arrow(pAudioPlayerItem, bArrowUp);
			break;
		case PROP_SHOW_ARROW:
			bShowArrow = g_value_get_boolean(pValue);
			if(bShowArrow)
				clutter_actor_show (priv->pArrow);
			else
				clutter_actor_hide (priv->pArrow);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : frampton_audio_player_item_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void frampton_audio_player_item_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec)
{
        FramptonAudioPlayerItem *pAudioPlayerItem = FRAMPTON_AUDIO_PLAYER_ITEM(pObject);
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pAudioPlayerItem);

	switch (uinPropertyID)
	{
		case PROP_ICON:
                        g_value_set_string (pValue, priv->pThumbNailPath);
                        break;
                case PROP_MID_TEXT:
			{
				const gchar *text = clutter_text_get_text (CLUTTER_TEXT (priv->pMidText));
                        	g_value_set_string (pValue, text);
			}
                        break;
                case PROP_RIGHT_TEXT:
			{
				const gchar *text = clutter_text_get_text (CLUTTER_TEXT (priv->pRightText));
                        	g_value_set_string (pValue, text);
			}
                        break;
		case PROP_ARROW_UP:
			g_value_set_boolean (pValue, priv->bArrowUp);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}
/********************************************************
 * Function : frampton_audio_player_item_finalize
 * Description: Finalize the meta info heade object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void frampton_audio_player_item_finalize (GObject *pObject)
{
	G_OBJECT_CLASS (frampton_audio_player_item_parent_class)->finalize (pObject);
}


/********************************************************
 * Function : frampton_audio_player_item_dispose
 * Description: Dispose the info roller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void frampton_audio_player_item_dispose (GObject *pObject)
{
	FramptonAudioPlayerItem *self = FRAMPTON_AUDIO_PLAYER_ITEM (pObject);
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (self);

	g_clear_pointer (&priv->pFooterImagePath, g_free);
	g_clear_pointer (&priv->pArrowUpMarkup, g_free);
	g_clear_pointer (&priv->pArrowDownMarkup, g_free);
	g_clear_pointer (&priv->pFont, g_free);
	g_clear_pointer (&priv->pThumbNailPath, g_free);

	g_clear_object (&priv->glow_effect_1);
	g_clear_object (&priv->glow_effect_2);

	G_OBJECT_CLASS (frampton_audio_player_item_parent_class)->dispose (pObject);

}

/********************************************************
 * Function : frampton_audio_player_item_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void frampton_audio_player_item_class_init (FramptonAudioPlayerItemClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);

	GParamSpec *pspec = NULL;

	pObjectClass->get_property = frampton_audio_player_item_get_property;
	pObjectClass->set_property = frampton_audio_player_item_set_property;
	pObjectClass->dispose = frampton_audio_player_item_dispose;
	pObjectClass->finalize = frampton_audio_player_item_finalize;

	 /**
         * FramptonAudioPlayerItem:icon:
         *
         * image path
         *
         * Default: NULL
         */
	pspec = g_param_spec_string ("icon",
                              "Thumb-Icon",
                              "texture path for the thumbnail icon",
                              NULL,
                              G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_ICON, pspec);

	 /**
         * FramptonAudioPlayerItem:image-data:
         *
         * Content actor for image creation
         *
         * Default: NULL
	 */
	pspec = g_param_spec_object ("image-data",
                              "Image-Data",
                              "texture content for the thumbnail icon",
                              G_TYPE_OBJECT,
                              G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_CONTENT, pspec);

	/**
         * FramptonAudioPlayerItem:mid-text:
         *
         * Middle text for the item
         *
         * Default: NULL
         */
	pspec = g_param_spec_string ("mid-text",
                               "Mid-Text",
                               "Mid-Text for roller item",
                               NULL,
                               G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_MID_TEXT, pspec);

	/**
         * FramptonAudioPlayerItem:right-text:
         *
         * Right text for the item
         *
         * Default: NULL
         */
	pspec = g_param_spec_string ("right-text",
                               "Right-Text",
                               "Right-Text for roller item",
                               NULL,
                               G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_RIGHT_TEXT, pspec);


	/**
         * FramptonAudioPlayerItem:row:
         *
         * item row number
         *
         * Default: 0
         */
	pspec = g_param_spec_uint ("row",
                        "row number",
                        "row number",
                        0, G_MAXUINT,
                        0,
                        G_PARAM_WRITABLE);
        g_object_class_install_property (pObjectClass, PROP_ROW, pspec);

	/**
         * FramptonAudioPlayerItem:focused:
         *
         * Whether this actor should be rendered as focused
         *
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("focused",
                        "focused",
                        "Whether this actor should be rendered as focused",
                        FALSE,
                        G_PARAM_WRITABLE);
        g_object_class_install_property (pObjectClass, PROP_FOCUSED, pspec);

	/**
         * FramptonAudioPlayerItem:footer:
         *
         * Whether this item is for roller Footer
         *
         * Default:
         */
        pspec = g_param_spec_boolean ("footer",
                        "Footer",
                        "Whether this item is for roller Footer",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_FOOTER, pspec);

	/**
         * FramptonAudioPlayerItem:arrow-up:
         *
         * Whether footer arrow direction is up or down
         *
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("arrow-up",
                        "Arrow-Up",
                        "Whether footer arrow direction is up or down",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_ARROW_UP, pspec);

	/**
         * FramptonAudioPlayerItem:show-arrow:
         *
         * Whether footer arrow needs to be shown or hidden
         *
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("show-arrow",
                        "Arrow-Show",
                        "Whether footer arrow needs to be shown",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_SHOW_ARROW, pspec);

}

/********************************************************
 * Function : frampton_audio_player_item_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void frampton_audio_player_item_init (FramptonAudioPlayerItem *pSelf)
{
	FramptonAudioPlayerItemPrivate *priv = frampton_audio_player_item_get_instance_private (pSelf);
        GHashTable *pStyleHash;

	priv->pThumbNail = NULL;
	priv->pMidText = NULL;
	priv->pRightText = NULL;
	priv->pVerticalLine = NULL;
	priv->pBottomLine = NULL;
	priv->pFooterBg = NULL;
	priv->pThumbNailPath = NULL;
	priv->pArrow = NULL;
	priv->row_called = FALSE;

	priv->bFooter = FALSE;

	pStyleHash = thornbury_style_set (PKGDATADIR"/mh_ap_item_style.json");

        /* pares the hash for styles */
        if (NULL != pStyleHash)
        {
                GHashTableIter iter;
                gpointer key, value;

                g_hash_table_iter_init(&iter, pStyleHash);
                /* iter per layer */
                while (g_hash_table_iter_next(&iter, &key, &value))
                {
                        GHashTable *pHash = value;
                        if (NULL != pHash)
                        {
                                g_hash_table_foreach(pHash, v_audio_player_item_parse_style,
                                                     pSelf);
                        }
                }
        }
        /* free the style hash */
        thornbury_style_free(pStyleHash);

	/* create separator */
	priv->pVerticalLine = draw_line (priv->flVerticalLineX, 0, 1, priv->flIconHeight, priv->lineColor);
	clutter_actor_add_child (CLUTTER_ACTOR (pSelf), priv->pVerticalLine);
	/* create bottom separator */
	priv->pTopLine = draw_line (0, 1, priv->flHorizontalLineWidth, 1, priv->lineColor);
	clutter_actor_add_child (CLUTTER_ACTOR (pSelf), priv->pTopLine);

	/* create last bottom separator */
	priv->pBottomLine = draw_line (0, priv->flBottomLineY, priv->flHorizontalLineWidth, 1, priv->lineColor);
	clutter_actor_add_child (CLUTTER_ACTOR (pSelf), priv->pBottomLine );

	if (NULL != priv->pThumbNailPath)
	{
		priv->pThumbNail = thornbury_ui_texture_create_new (priv->pThumbNailPath, priv->flIconWidth, priv->flIconHeight, TRUE, FALSE);
		clutter_actor_add_child (CLUTTER_ACTOR (pSelf), priv->pThumbNail);
		clutter_actor_set_position (priv->pThumbNail, 0, 1);
	}

	/* content box */
	priv->pBox = clutter_actor_new();
	clutter_actor_set_size (priv->pBox, 64, 64);
	clutter_actor_add_child (CLUTTER_ACTOR (pSelf), priv->pBox);

	/* create mid text */
	priv->pMidText = clutter_text_new ();
        clutter_actor_add_child (CLUTTER_ACTOR (pSelf), priv->pMidText);

        g_object_set (priv->pMidText,
		"font-name", priv->pFont,
		"color", &priv->textColor,
	 	"reactive", FALSE,
		"ellipsize", PANGO_ELLIPSIZE_END,
		NULL);

	/* create mid text */
        priv->pRightText = clutter_text_new ();
        clutter_actor_add_child (CLUTTER_ACTOR (pSelf), priv->pRightText);

        g_object_set(priv->pRightText,
		"font-name", priv->pFont,
		"color", &priv->textColor,
		"reactive", FALSE,
		"ellipsize", PANGO_ELLIPSIZE_END,
		NULL);

	clutter_actor_set_reactive (CLUTTER_ACTOR (pSelf), TRUE);

	priv->glow_effect_1 = g_object_ref_sink (lightwood_glow_shader_new ());
	priv->glow_effect_2 = g_object_ref_sink (lightwood_glow_shader_new ());
}

/**
 * frampton_audio_player_item_new:
 * Returns: audio player item object
 *
 * Creates a audio player item object
 */
FramptonAudioPlayerItem *frampton_audio_player_item_new (void)
{
	return g_object_new (FRAMPTON_TYPE_AUDIO_PLAYER_ITEM, NULL);
}
