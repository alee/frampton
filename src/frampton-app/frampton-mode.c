/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "frampton-mode.h"

#include "frampton.h"

/*
 * FramptonMode:
 *
 * Object representing one of the modes/views with which Frampton can run:
 * Artists, Albums or Songs. The remaining mode (Frampton itself, used
 * for URI handling) is represented by FramptonApp.
 */

struct _FramptonMode
{
  GApplication parent;
};

G_DEFINE_TYPE (FramptonMode, frampton_mode, G_TYPE_APPLICATION)

static void
frampton_mode_class_init (FramptonModeClass *klass)
{
}

static void
frampton_mode_init (FramptonMode *self)
{
}

FramptonMode *
frampton_mode_new (FramptonAudioPlayerMode mode)
{
  const gchar *id;

  switch (mode)
    {
    case FRAMPTON_AUDIO_PLAYER_MODE_ALBUM:
      id = FRAMPTON_APP_ID ".Albums";
      break;

    case FRAMPTON_AUDIO_PLAYER_MODE_ARTIST:
      id = FRAMPTON_APP_ID ".Artists";
      break;

    case FRAMPTON_AUDIO_PLAYER_MODE_SONGS:
      id = FRAMPTON_APP_ID ".Songs";
      break;

    case FRAMPTON_AUDIO_PLAYER_MODE_NONE:
    default:
      g_return_val_if_reached (NULL);
    }

  return FRAMPTON_MODE (g_object_new (FRAMPTON_TYPE_MODE,
                                     "application-id", id,
                                     "flags", G_APPLICATION_FLAGS_NONE,
                                     NULL));
}
