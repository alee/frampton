
/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

typedef enum
{
  FRAMPTON_AUDIO_PLAYER_MODE_ARTIST = 0,
  FRAMPTON_AUDIO_PLAYER_MODE_ALBUM,
  FRAMPTON_AUDIO_PLAYER_MODE_SONGS,
  FRAMPTON_AUDIO_PLAYER_MODE_NONE
} FramptonAudioPlayerMode;

#define FRAMPTON_TYPE_MODE (frampton_mode_get_type ())
G_DECLARE_FINAL_TYPE (FramptonMode, frampton_mode, FRAMPTON, MODE,
                      GApplication)

FramptonMode *frampton_mode_new (FramptonAudioPlayerMode mode);

G_END_DECLS
